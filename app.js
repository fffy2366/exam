
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var admin = require('./routes/admin');
var adminPaper = require('./routes/admin.paper');
var adminGraded = require('./routes/admin.graded');
var adminClassification = require('./routes/admin.classification');
var adminExcel = require('./routes/admin.excel');
var adminMusic = require('./routes/admin.music');
var adminCategory = require('./routes/admin.category');
var adminNews = require('./routes/admin.news');
var adminCode = require('./routes/admin.code');
var adminLoginlog = require('./routes/admin.loginlog');

var user = require('./routes/user');
var homes = require('./routes/home');
var tests = require('./routes/test');
var frees = require('./routes/free');
var sockets = require('./routes/socket');
var http = require('http');
var path = require('path');
var logger = require('./Logger') ;

var settings = require('./settings') ;
var crypto = require('crypto') ;
var flash = require('connect-flash');

var Options = require('./models/options') ;
var app = express();

// all environments

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}



app.configure(function(){ 
	app.set('views', __dirname + '/views'); 
	//app.use(express.bodyParser()); 
	app.use(express.bodyParser({keepExtensions: false,uploadDir:'./public/uploads'})) ;
	app.use(express.methodOverride()); 
	app.use(express.cookieParser()); 

	app.set('port', process.env.PORT || 3000);
	app.set('view engine', 'ejs');
	app.use(express.favicon(path.join(__dirname, './public/img/favicon.ico')));
	//app.use(app.router);
	app.use('/public',express.static(path.join(__dirname, '/public')));
	app.use('/resources',express.static(path.join(__dirname, '/resources')));
	app.use('/mobile/',express.static(path.join(__dirname, '/mobile/')));
	app.use(express.logger('dev'));
	app.use(express.session({
		secret: settings.cookieSecret
		
	})); 
	
	app.use(express.cookieParser('keyboard cat'));
	app.use(express.session({ cookie: { maxAge: 60000 }}));
	app.use(flash());	

	
	app.use(function(req, res, next){
//		req.session.admin = {id:1,email:'fengxuting@qq.com',username:'frank'};
		//req.session.user = {user_id:2,mobile:'13001047235',nickname:'test2',power:510,class_power:16};
		//req.session.user = {user_id:1,mobile:'13621370125',nickname:'就爱矿泉水'};
		res.locals.admin =  req.session.admin;
		res.locals.user = req.session.user;
		res.locals.error = req.flash('error')||{}; 
		res.locals.success = req.flash('success')||{}; 
		//res.locals.sitename = "音乐等级考试"; 
		logger.info("1.=============") ;
		var url = req.originalUrl;
		if (url != "/admin/login" && url != "/admin/loginAction" && !(url.indexOf('/admin/mobileCode')>-1) &&(url.indexOf('/admin')>-1) &&!req.session.admin) {
		    return res.redirect("/admin/login");
		}
		if (url != "/" &&url != "/login" &&url != "/landing" &&url != "/logout" &&url != "/contactus" &&url != "/aboutus" && url != "/loginAction"&& !(url.indexOf('/socket')>-1)&& !(url.indexOf('/free')>-1)&&!(url.indexOf('/captcha')>-1)&& !(url.indexOf('/reg')>-1) && !(url.indexOf('/mobileCode')>-1) &&!(url.indexOf('/admin')>-1) &&!(url.indexOf('/mobile')>-1) &&!req.session.user) {
		    return res.redirect("/login");
		}
		if(req.session.sitename){
			res.locals.sitename = req.session.sitename; 
			next() ;
		}else{
			Options.findByName('sitename', function(err, option){
				if(err){
					console.log("err") ;
				}
				req.session.sitename = option[0].option_value;
				res.locals.sitename = req.session.sitename; 
				next();
			}) ;			
		}
		
	});	
});


routes(app);
admin(app);
adminPaper(app);
adminGraded(app);
adminClassification(app);
adminExcel(app) ;
adminMusic(app) ;
adminCategory(app) ;
adminNews(app) ;
adminCode(app) ;
adminLoginlog(app) ;
homes(app);
tests(app);
frees(app);
sockets(app) ;
var server = http.createServer(app) ;
var io = require('socket.io').listen(server) ;
//var io = require('socket.io')() ;
/*
io.configure('production', function(){
      io.enable('browser client etag');
      io.set('log level', 1);

      io.set('transports', [
        'websocket'
      , 'flashsocket'
      , 'htmlfile'
      , 'xhr-polling'
      , 'jsonp-polling'
      ]);
    });
*/
io.sockets.on('connection', function (socket) {
	socket.emit('news', { hello: 'world' });
	socket.on('my other event', function (data) {
		console.log(data);
	});
});

server.listen(app.get('port'), function(){
  logger.info('Express server listening on port ' + app.get('port'));
});


