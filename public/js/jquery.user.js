(function($){
	/**
	 * 计时
	 */
	function time() {
		$('#sendSms').html(60) ;
		var i=59;
		function c(){
			if(i>0){
				//$('#sendSms').html(i);
				//$('#sendSms').val(i+"秒后重发");
				//$('#sendSms').val("24小时内只能发送一次") ;
				i--;
				setTimeout(c,1000) ;
			}else{
				//$('#sendSms').html("发送") ;
				$('#sendSms').val("发送手机验证码") ;
		        $("#sendSms").on('click',function(){
		        	$this = $(this) ;
		        	sendSms($this) ;
		        }) ;
			}
		}
		c() ;
	}
	//发送验证码
	var sendSms = function(type,$this){
    	var mobile = $("#mobile").val() ;
        var code = $("#code").val() ;
    	if(mobile==""){
    		alert("请先输入手机号") ;
    		return ;
    	}
        if(code==""){
            alert("请输入下方图片验证码") ;
            return ;
        }
		mobile = $.trim(mobile) ;
		if(!mobile.match(/^1[3|4|5|7|8][0-9]\d{4,8}$/)){
			alert("手机号格式不正确") ;
			return ;
		}
		//倒计时
		//time() ;
		//$this.unbind('click') ;
		//return ;

    	$.post('/mobileCode?mobile='+mobile+"&t="+Math.random(),{'type':type,code:code},function(data){
    		if(data=="success"){
                $("#captcha").attr('src','/captcha?'+Math.random()) ;
    			alert('发送成功') ;
    			return ;
    		}
            if(data=="codeeror"){
                alert('图片验证码错误') ;
                $("#captcha").attr('src','/captcha?'+Math.random()) ;
                return ;
            }
            if(data.status=="n"&&data.info=="lt24h"){
                alert("您24小时内只能发送"+data.captcha_count+"次验证码") ;
            }
            if(data.status=="n"&&data.info=="lt20"){
                alert("休息一下明天再发吧") ;
            }

    	}) ;

	} ;
	var login = function(){
		var mobile = $("#mobile").val() ;
		var password = $("#password").val() ;
		var code = $("#code").val() ;
		var mobileCode = $("#mobileCode").val() ;
    	if(mobile==""){
    		alert("请先输入手机号") ;
    		return ;
    	}
		mobile = $.trim(mobile) ;
		if(!mobile.match(/^1[3|4|5|7|8][0-9]\d{4,8}$/)){
			alert("手机号格式不正确") ;
			return ;
		}
    	if(password==""){
    		alert("请输入密码") ;
    		return ;
    	}
    	if(code==""){
    		alert("请输入验证码") ;
    		return ;
    	}
    	if(mobileCode==""){
    		//alert("请输入手机验证码") ;
    		//return ;
    	}
    	var data = {'mobile':mobile,password:password,code:code,mobileCode:mobileCode} ;
    	$.post('/login',data,function(data){
    		if(data=="codeeror"){
    			$("#captcha").attr('src','/captcha?'+Math.random()) ;
    			alert("验证码错误") ;
    		}
    		if(data=="mobileCodeError"){
    			alert("手机验证码错误") ;
    		}
            if(data=="passworderror"){
                alert("账户或密码错误") ;
            }
            if(data=="pwderr"){
                alert("账户或密码错误") ;
            }
            if(data=="todayhasotheriplogin"){
                alert("一天只能一个ip登录") ;
            }
            if(data=="nofee"){
                alert("您还不是会员哦") ;
            }
    		if(data=="success"){
    			window.location.href = "/list/fenji" ;
    		}
    	}) ;
	}
    var reg = function(){
        var mobile = $("#mobile").val() ;
        var mobileCode = $("#mobileCode").val() ;
        var nickname = $("#nickname").val() ;
        var taobao = $("#taobao").val() ;
        var alipay = $("#alipay").val() ;
        var password = $("#password").val() ;
        var rpassword = $("#rpassword").val() ;
        var code = $("#code").val() ;
        var exam_time = $("#exam_time").val() ;
        var address = $("#address").val() ;
        var is_read = $("#read").attr("checked") ;
        if(mobile==""){
            alert("请先输入手机号") ;
            return ;
        }
        mobile = $.trim(mobile) ;
        if(!mobile.match(/^1[3|4|5|7|8][0-9]\d{4,8}$/)){
            alert("手机号格式不正确") ;
            return ;
        }
        if(mobileCode==""){
            alert("请输入手机验证码") ;
            return ;
        }
        if(nickname==""){
            alert("请输入昵称") ;
            return ;
        }
        if(taobao==""){
            //alert("请输入淘宝账号") ;
            //return ;
        }
        if(password==""){
            alert("请输入密码") ;
            return ;
        }
        if(rpassword==""){
            alert("请输入确认密码") ;
            return ;
        }
        if(password!=rpassword){
            alert("两次密码输入不一致") ;
            return ;
        }
        if(code==""){
            alert("请输入验证码") ;
            return ;
        }
        if(is_read!='checked'){
            alert("同意注册协议才能注册哦") ;
            return ;
        }
        var data = {'mobile':mobile,mobileCode:mobileCode,nickname:nickname,taobao:taobao,alipay:alipay,password:password,code:code,exam_time:exam_time,address:address} ;
        $.post('/reg',data,function(data){
            if(data=="codeeror"){
                $("#captcha").attr('src','/captcha?'+Math.random()) ;
                alert("验证码错误") ;
            }
            if(data=="mobileCodeError"){
                alert("手机验证码错误") ;
            }
            if(data=="mobileexist"){
                alert("该手机已经注册过") ;
            }

            if(data=="todayhasotheriplogin"){
                alert("一天只能一个ip登录") ;
            }

            if(data=="success"){
                //alert("祝贺您已经注册成功，但还不能马上进入模拟测试，为了不耽误您的练习，请尽快联系我们，并付款。") ;
                alert("您已注册成功，如果已经付款，请联系客服及时为您开通。如果还未付款，请到淘宝：音乐大世界 http://shop110434287.taobao.com 进行付款") ;
                window.location.href = "/login" ;
            }
        }) ;
    } ;
    $(function(){
        //$('get')
        //$.get('') ;
        $("#captcha").css({'cursor':'pointer','padding-bottom_':'10px'}).attr('src','/captcha?'+Math.random()).click(function(){
            $(this).attr('src','/captcha?'+Math.random()) ;
        }) ;
        $("#sendSms").on('click',function(){
        	$this = $(this) ;
        	sendSms('sms',$this) ;
        }) ;
		$("#sendVoice").on('click',function(){
			$this = $(this) ;
			sendSms('voice',$this) ;
		}) ;
        $("#loginBtn").click(function(){
            login() ;
        }) ;
        $("#reg").click(function(){
            reg() ;
        }) ;
    }) ;
})(jQuery);
