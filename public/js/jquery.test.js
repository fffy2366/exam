var timeID;
var timePlay;
var timePause = true ;//计时器状态
var timeCount = 0 ;

$(function(){
	$.history.init(function (hash) {
		var msg;
		if (hash){
	　　　msg= hash;
　　　　	}else{
　　　　　　	msg= ""
　　　　	}
		if(msg==""){
			msg = 1 ;
		}
		question(paper_id,msg) ;
		$(".title-sound").live('mouseover',function(){
			$(".title-sound").find("span").css({'background-position':'-40px -80px'}) ;
		}) ;
		$(".title-sound").live('mouseout',function(){
			$(".title-sound").find("span").css({'background-position':'-40px 0px'}) ;
		}) ;
		$(".title-sound span").live('click',function(){
			play("title_audio","autoplay",1,0) ;
		}) ;
		
		
　　});

	$("#pause").click(function(){
		stopTime() ;
	}) ;
});
/**
 * 音乐播放
 */
var play = function(media_id,auto,count,interval){
	window.console&&console.log(media_id+" "+auto+" "+count+" "+interval) ;
	var totalCount = count ;
	var Media = document.getElementById(media_id); 
	Media.play();    //播放  
	eventTester = function(e){
		Media.addEventListener(e,function(){  
        	window.console&&console.log((new Date()).getTime(),e); 
        	count-- ;        		
        	if(count>0){
        		timePlay = setTimeout(function(){
		        	Media.play();    //播放   
		        	
		        	$("#play_times").html(totalCount-count+1) ;
        		},interval*1000) ;
        	}
       });  
   }
   eventTester("ended"); 

}
var question = function(paper_id,id){
	clearTimeout(timePlay);

	$.post(ajaxUrl.paperquestion,{paper_id:paper_id,id:id},function(data){
		if(data==""){
			return ;
		}
		window.console&&console.log(data.answers[1]) ;
		window.console&&console.log(data) ;
		var autoplay = !!data.settings&&eval('('+data.settings+')').autoplay=="yes"?"autoplay":"" ;
		var title_autoplay = !!data.settings&&eval('('+data.settings+')').titlesound_autoplay=="yes"?"autoplay":"" ;
		var question_img = !!data.image?'<img src="/public/uploads/questions/'+data.image+'" />':'' ;
		var question_audio = !!data.sound?'<audio id="audio" style="width:290px;height:30px" controls="controls" src="/public/uploads/musics/'+data.sound+'"></audio>':'' ;
		var titlesound = !!data.titlesound?'<audio id="title_audio"  src="/public/uploads/titlesounds/'+data.titlesound+'"></audio>':'' ;

		var answers_a_img = !!data.answers[0].img?'<img style="" src="/public/uploads/options/'+data.answers[0].img+'" />':'' ;
		var answers_b_img = !!data.answers[1].img?'<img style="" src="/public/uploads/options/'+data.answers[1].img+'" />':'' ;
		var answers_c_img = !!data.answers[2].img?'<img style="" src="/public/uploads/options/'+data.answers[2].img+'" />':'' ;
		var answers_d_img = !!data.answers[3].img?'<img style="" src="/public/uploads/options/'+data.answers[3].img+'" />':'' ;
		var answers_a_title = !!data.answers[0].title?data.answers[0].title:'' ;
		var answers_b_title = !!data.answers[1].title?data.answers[1].title:'' ;
		var answers_c_title = !!data.answers[2].title?data.answers[2].title:'' ;
		var answers_d_title = !!data.answers[3].title?data.answers[3].title:'' ;
		
		var html = ""+
		'<input type="hidden" name="question_id" id="question_id" value="'+data.question_id+'"/>'+'<input type="hidden" name="question_id" id="question_id" value="'+data.question_id+'"/>'+
		'<div class="timu">' ;
		if(!!data.titlesound){
			html+='<a class="title-sound"><span></span></a>'+titlesound+'' ;
		}			
		html +=""+
		'<p><span>'+id+'.</span>'+data.title+question_audio ;
		if(!!data.sound){
			html+='[第<span id="play_times">1</span>遍]' ;
		}	
		html +=""+
		'</p>'+
		''+question_img+''+
		'</div>'+
	
		'<div class="xuxian"></div>'+
	
		'<form action="" method="post" name="answer">'+
		'<p>'+
		'<input type="radio" name="answers" value="A"/>A.'+answers_a_title+'</p>'+
		''+answers_a_img+
		'<p>'+
		'<input type="radio" name="answers" value="B"/>B.'+answers_b_title+'</p>'+
		''+answers_b_img+
		'<p>'+
		'<input type="radio" name="answers" value="C"/>C.'+answers_c_title+'</p>'+
		''+answers_c_img+
		'<p>'+
		'<input type="radio" name="answers" value="D"/>D.'+answers_d_title+'</p>'+
		''+answers_d_img+
		'</form>'+
		'<div class="xuxian"></div>' ;
		$("#showanswer").html("") ;
		$(".shiti").html(html) ;

		showanswer(paper_id,data.question_id) ;

		var autoplay = !!data.settings&&eval('('+data.settings+')').autoplay=="yes"?"autoplay":"" ;

		var auto = "autoplay" ;
		var count = !!data.settings?eval('('+data.settings+')').playcount:1 ;
		var t = !!data.settings&&eval('('+data.settings+')').countdown?eval('('+data.settings+')').countdown:0 ;
		//t = 5 ;
		clearTimeout(timeID);
		if(t>0){
			showtime(t,id) ;//显示倒计时
		}else{
			$('#shower').html('')
		}
		var interval = 9 ;//间隔（秒）

		if(!!data.titlesound){
			if(title_autoplay=="autoplay"){
				play("title_audio","autoplay",1,0) ;
			}
		}		
		if(!!data.sound){
			if(title_autoplay=="autoplay"&&autoplay=="autoplay"){
				var Media = document.getElementById("title_audio"); 
				eventTester = function(e){
					Media.addEventListener(e,function(){
						play("audio",autoplay,count,interval) ;
					});  
				}
			    eventTester("ended"); 
			}else if(title_autoplay!="autoplay"&&autoplay=="autoplay"){
				play("audio",autoplay,count,interval) ;
			}
		}

		if(!!data.respondents&&!!data.respondents.answer){
			var has_answer = data.respondents.answer ;
			window.console&&console.log(has_answer) ;
			$("input[name='answers'][value='"+has_answer+"']").attr('checked',true) ;
		}
		$(".prete").find("a").attr('href','javascript:prev('+id+');') ;
		$(".tijiao").find("a").attr('href','javascript:tijiao('+id+');') ;
		$(".nextte").find("a").attr('href','javascript:next('+id+');') ;
	}) ;
}
var showanswer = function(paper_id,id){
	$("input[name=answers]").click(function(){
		var v = $(this).val() ;
		//window.console&&console.log("answer:"+v) ;
		//window.console&&console.log("paper_id:"+paper_id) ;
		//window.console&&console.log("id:"+id) ;
		$.post(ajaxUrl.showanswer,{paper_id:paper_id,question_id:id,option:v},function(data){
			if(data==""){
				return ;
			}
			if(data.ret=="1"){
				//alert("right") ;
				$("#showanswer").html("恭喜你，答对了") ;
			}else if(data.ret=="2"){
				//alert(data.right) ;
				$("#showanswer").html("答错了，正确答案是"+data.right) ;
			}
		});
	}) ;
}

/**
 * 上一题
 */
var prev = function(id){
	var prev_id = id-1 ;
	var answers = $("input[name='answers']:checked").val() ;
	if(id<2){
		save(answers) ;
		alert("这已经是第一题了") ;
		return ;
	}else{
		if(!!!answers){
			$.history.load(prev_id) ;	
			//alert("请选择答案") ;
			return ;
		}else{
			save(answers,prev_id) ;
		}		
	}
	
}
/**
 * 下一题
 */
var next = function(id){
	var next_id = parseInt(id)+1 ;
	var answers = $("input[name='answers']:checked").val() ;
	window.console&&console.log("paperCount:"+paperCount) ;
	window.console&&console.log("id:"+id) ;
	if(id>=paperCount){
		if(!!answers){
			save(answers) ;
		}
		//alert("这已经是最后一题了") ;
		var answers = $("input[name='answers']:checked").val() ;
		save(answers,'',ajaxUrl.testresult+'/'+paper_id) ;
		return ;
	}else{
		if(!!!answers){
			$.history.load(next_id) ;
			//alert("请选择答案") ;
			return ;
		}else{
			save(answers,next_id) ;
		}
	}
	window.console&&console.log(answers) ;

	return;
}
/**
 * 提交答卷
 */
var tijiao = function(id){
	var answers = $("input[name='answers']:checked").val() ;
	//if(!!answers){
		save(answers,'',ajaxUrl.testresult+'/'+paper_id) ;
	//}
	
}
/**
 * 保存当前题目
 */
var save = function(answers,jump_id,jump_url){
	$.post(ajaxUrl.respondents,{paper_id:paper_id,question_id:$("#question_id").val(),option:answers},function(data){
		if(jump_id){
			$.history.load(jump_id) ;			
		}
		if(jump_url){
			window.location.href=jump_url ;
		}
	}) ;
}


//倒计时
function showtime(t,curr_id){
	if(t<0){
		t = 0 ;
		next(curr_id) ;		
		return ;
	}
	if(t>10){
		$('#shower').css({'color':'#000'}) ;
	}
	var hh = t / 3600;
	hh = Math.floor(hh) ;
	var mm = (t - hh * 3600)/60 ;
	mm = Math.floor(mm) ;
	var ss = t - hh * 3600 - mm * 60;
	ss = Math.floor(ss) ;
	if(hh == 0 && mm == 0 && ss==10){
		//alert("还剩10秒") ;
		$('#shower').css({'color':'red'}) ;
	}
	//下一题
	if(hh == 0 && mm == 0 && ss== 0){
		
	}
	var stringhh = hh < 10 ? '0'+hh : hh;
	var stringmm = mm < 10 ? '0'+mm : mm;
	var stringss = ss < 10 ? '0'+ss : ss;
	
	var left_time = stringmm +":"+ stringss ;
	if(hh>=0){
		left_time = stringhh+":"+ stringmm +":"+ stringss ;
	}
	$('#shower').html(left_time)  ;
	t = t -1;
	timePause = false ;
	timeCount = t ;
	timeID = setTimeout(function(){
		showtime(t,curr_id) ;
	},1000) ;
}

var stopTime = function(){
	if(timePause){
		timePause = false ;
		showtime(timeCount,paper_id) ;
	}else{
		timePause = true ;
		clearTimeout(timeID) ;
	}

}