var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var News = function() {};

News.prototype.find = function(id, callback) {
    var sql = "SELECT * FROM news WHERE id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 添加
 */
News.save = function(data, callback) {
    var keys = Object.keys(data);
    var conditions_key = [] ;
    var conditions_val = [] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        for (var i = 0; i < keys.length; i++) {
            //var val = obj[keys[i]];
            // use val
            conditions_key.push(keys[i]) ;
            conditions_val.push(connection.escape(data[keys[i]])) ;
        }
        var key_str = conditions_key.join(" , ") ;
        var val_str = conditions_val.join(" , ") ;
        var sql = "INSERT INTO news ( "+key_str+" ) VALUES("+val_str+")";
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 更新
 */
News.update = function(id, data, callback) {
    logger.info(" id:"+id) ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        var sql = "UPDATE news SET ";
        var conditions = [] ;
        var keys = Object.keys(data);
        for (var i = 0; i < keys.length; i++) {
            conditions.push(keys[i]+" = "+connection.escape(data[keys[i]])) ;
        }
        var conditions_str = conditions.join(" , ") ;
        sql += conditions_str ;
        sql += " WHERE id =?" ;
        // make the query
        connection.query(sql,[parseInt(id)], function(err, results) {
            if (err) {
                logger.info("err:"+err) ;
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};

News.findAll = function(callback){
    var sql = "SELECT  * FROM news WHERE 1=1 AND is_deleted = 0 ";
   
    sql += " ORDER BY created_at ASC " ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
			connection.release();
			callback(false, results);            
        });
    });
}
News.findAllByPage = function(cate_id,currPage, pageSize, keywords, callback){
    var currCount = (parseInt(currPage)-1)*pageSize ;
    var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM news WHERE 1=1 AND is_deleted = 0";
    if(cate_id&&cate_id==24){
        sql += " AND (cate_id =2 OR cate_id=4) " ;
    }else{
        sql += " AND cate_id = "+cate_id ;
    }
    if(keywords){
        sql += " AND title LIKE '%"+keywords+"%'" ;
    }
    sql += " ORDER BY created_at DESC LIMIT "+currCount+","+pageSize ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
            var sqlCount = "SELECT FOUND_ROWS() c" ;
            connection.query(sqlCount, function(err, totalCount){
                //console.log(totalCount) ;
                connection.release();
                callback(false, totalCount[0].c, results);
            }) ;
        });
    });    
}
News.findById = function(id, callback){
    var sql = "SELECT * FROM news WHERE id=?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [parseInt(id)], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info("err:"+err) ;
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
News.deleteById = function(id, callback){
    logger.info(" id:"+id) ;
    var sql = "UPDATE news SET is_deleted = 1 WHERE id=? ";
    var conditions = [parseInt(id)] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}

module.exports = News ;
