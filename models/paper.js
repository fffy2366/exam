var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var Paper = function() {};

Paper.prototype.find = function(id, callback) {
    var sql = "SELECT * FROM papers WHERE pager_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 添加
 */
Paper.save = function(title, is_free, is_music, graded_id,classification_id,description, created_at, callback) {
    logger.info("title:"+title) ;
    var sql = "INSERT INTO papers (title,is_free,is_music,graded_id,classification_id, description, created_at) VALUES(?,?,?,?,?,?,?)";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [title,is_free,is_music,graded_id,classification_id, description, created_at], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 更新
 */
Paper.update = function(id, title,is_free,is_music,graded_id,classification_id, description, callback) {
    logger.info("title:"+title+" id:"+id) ;
    var sql = "UPDATE papers SET title = ?,is_free = ? , is_music = ? , graded_id = ?, classification_id = ?, description = ? WHERE paper_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            console.log("err:"+err) ;
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [title, is_free, is_music, graded_id,classification_id,description, parseInt(id)], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                console.log("err:"+err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
Paper.findAll = function(callback){
    var sql = "SELECT  * FROM papers WHERE 1=1 ";
   
    sql += " ORDER BY created_at DESC " ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
			connection.release();
			callback(false, results);            
        });
    });    
}
Paper.findAllByPage = function(currPage, pageSize, keywords, type, tval, callback){
    var currCount = (parseInt(currPage)-1)*pageSize ;
    var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM papers WHERE 1=1 ";
    if(keywords){
        sql += " AND title LIKE '%"+keywords+"%' AND description LIKE '%"+keywords+"%'" ;
    }
    if(type){
        sql += "AND "+type+"_id ="+tval ;
    }
    //初级排除免费试用132
    sql += " AND is_free=0" ;
    sql += " AND user_id is null " ;
    sql += " ORDER BY created_at DESC LIMIT "+currCount+","+pageSize ;
    logger.info("Paper sql:"+sql) ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                console.log("find all err:"+err) ;
                connection.release();
                callback(true);
                return;
            }
            var sqlCount = "SELECT FOUND_ROWS() c" ;
            connection.query(sqlCount, function(err, totalCount){
                //console.log(totalCount) ;
                connection.release();
                callback(false, totalCount[0].c, results);
            }) ;
        });
    });    
}
/**
 * 免费题库
 */
Paper.findAllByPageIsFree = function(currPage, pageSize, keywords, type, tval, callback){
    var currCount = (parseInt(currPage)-1)*pageSize ;
    var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM papers WHERE 1=1 ";
    if(keywords){
        sql += " AND title LIKE '%"+keywords+"%' AND description LIKE '%"+keywords+"%'" ;
    }
    if(type){
        sql += "AND "+type+" ="+tval ;
    }
    sql += " ORDER BY created_at DESC LIMIT "+currCount+","+pageSize ;
    logger.info("Paper sql:"+sql) ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                console.log("find all err:"+err) ;
                connection.release();
                callback(true);
                return;
            }
            var sqlCount = "SELECT FOUND_ROWS() c" ;
            connection.query(sqlCount, function(err, totalCount){
                //console.log(totalCount) ;
                connection.release();
                callback(false, totalCount[0].c, results);
            }) ;
        });
    });    
}

Paper.findById = function(id, callback){
    var sql = "SELECT * FROM papers WHERE paper_id=?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
Paper.deleteById = function(pid, callback){
    logger.info(" pid:"+pid) ;
    var sql = "DELETE FROM papers WHERE paper_id=? ";
    var conditions = [pid] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}

module.exports = Paper ;
