var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var Option = function() {};

Option.prototype.save = function(id, callback) {
    var sql = "SELECT * FROM users WHERE user_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
        	logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
Option.findByName = function(name, callback){
    var sql = "SELECT * FROM options WHERE option_name = ?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [name], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 更新
 */
Option.update = function(name,value, callback) {
    logger.info("name:"+name+" value:"+value) ;
    var sql = "UPDATE options SET  option_value = ? WHERE option_name =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            console.log("err:"+err) ;
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [value, name], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                console.log("err:"+err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
module.exports = Option ;