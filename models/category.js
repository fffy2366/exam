var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var Category = function() {};

Category.prototype.find = function(id, callback) {
    var sql = "SELECT * FROM category WHERE category_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 添加
 */
Category.save = function(categorytitle, created_at, callback) {
    logger.info("categorytitle:"+categorytitle) ;
    var sql = "INSERT INTO category (title, created_at) VALUES(?,?)";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [categorytitle, created_at], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 更新
 */
Category.update = function(id, categorytitle, callback) {
    logger.info("categorytitle:"+categorytitle+" id:"+id) ;
    var sql = "UPDATE category SET title = ?  WHERE category_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [categorytitle, parseInt(id)], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
Category.findAll = function(callback){
    var sql = "SELECT  * FROM category WHERE 1=1 AND is_deleted = 0 ";
   
    sql += " ORDER BY created_at ASC " ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
			connection.release();
			callback(false, results);            
        });
    });
}
Category.findAllByPage = function(currPage, pageSize, keywords, callback){
    var currCount = (parseInt(currPage)-1)*pageSize ;
    var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM category WHERE 1=1 AND is_deleted = 0";
    if(keywords){
        sql += " AND title LIKE '%"+keywords+"%'" ;
    }
    sql += " ORDER BY created_at DESC LIMIT "+currCount+","+pageSize ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
            var sqlCount = "SELECT FOUND_ROWS() c" ;
            connection.query(sqlCount, function(err, totalCount){
                //console.log(totalCount) ;
                connection.release();
                callback(false, totalCount[0].c, results);
            }) ;
        });
    });    
}
Category.findById = function(id, callback){
    var sql = "SELECT * FROM category WHERE category_id=?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
Category.deleteById = function(tid, callback){
    logger.info(" tid:"+tid) ;
    var sql = "UPDATE category SET is_deleted = 1 WHERE category_id=? ";
    var conditions = [tid] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}

module.exports = Category ;
