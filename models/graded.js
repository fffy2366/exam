var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var Graded = function() {};

Graded.prototype.find = function(id, callback) {
    var sql = "SELECT * FROM graded WHERE graded_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 添加
 */
Graded.save = function(gradedname, created_at, callback) {
    logger.info("gradedname:"+gradedname) ;
    var sql = "INSERT INTO graded (name, created_at) VALUES(?,?)";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [gradedname, created_at], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 更新
 */
Graded.update = function(id, gradedname, callback) {
    logger.info("gradedname:"+gradedname+" id:"+id) ;
    var sql = "UPDATE graded SET name = ?  WHERE graded_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [gradedname, parseInt(id)], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
Graded.findAll = function(callback){
    var sql = "SELECT  * FROM graded WHERE 1=1 AND is_deleted = 0 ";
   
    sql += " ORDER BY created_at ASC " ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
			connection.release();
			callback(false, results);            
        });
    });
}
Graded.findAllByPage = function(currPage, pageSize, keywords, callback){
    var currCount = (parseInt(currPage)-1)*pageSize ;
    var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM graded WHERE 1=1 AND is_deleted = 0";
    if(keywords){
        sql += " AND name LIKE '%"+keywords+"%'" ;
    }
    sql += " ORDER BY created_at DESC LIMIT "+currCount+","+pageSize ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
            var sqlCount = "SELECT FOUND_ROWS() c" ;
            connection.query(sqlCount, function(err, totalCount){
                //console.log(totalCount) ;
                connection.release();
                callback(false, totalCount[0].c, results);
            }) ;
        });
    });    
}
Graded.findById = function(id, callback){
    var sql = "SELECT * FROM graded WHERE graded_id=?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
Graded.deleteById = function(tid, callback){
    logger.info(" tid:"+tid) ;
    var sql = "UPDATE graded SET is_deleted = 1 WHERE graded_id=? ";
    var conditions = [tid] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}

module.exports = Graded ;
