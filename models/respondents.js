/**
 * 保存答案
 */
var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var Respondents = function() {};

Respondents.prototype.find = function(id, callback) {
    var sql = "SELECT * FROM respondents WHERE id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};

Respondents.save = function(user_id, paper_id, question_id, option,created_at, callback) {
    logger.info("user_id:"+user_id+"paper_id:"+paper_id+"question_id:"+question_id+"created_at:"+created_at) ;
    var sql = "INSERT INTO respondents(user_id, paper_id, question_id, answer, created_at) VALUES(?,?,?,?,?)";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            if(connection){
                connection.release();
            }
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [user_id, paper_id, question_id, option,created_at], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info("err"+err) ;
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
Respondents.update = function(user_id, paper_id, question_id, option,created_at, callback) {
    logger.info("user_id:"+user_id+" paper_id:"+paper_id+"question_id:"+question_id+"==============") ;
    var query_sql = "SELECT * FROM respondents WHERE user_id = ? AND paper_id = ? AND question_id =? " ;
    var sql = "UPDATE respondents SET answer = ?  WHERE user_id = ? AND paper_id = ? AND question_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            if(connection){
                connection.release();
            }
            callback(true);
            return;
        }
        // make the query
        connection.query(query_sql, [user_id, paper_id, question_id], function(err, respondents) {
            if(err){
                logger.info(err) ;
                connection.release();
                callback(true);
                return ;
            }
            if (respondents.length>0) {
                connection.query(sql, [option,user_id, paper_id, question_id], function(err, results) {
                    logger.info("sql:"+sql) ;
                    if (err) {
                        logger.info(err) ;

                        connection.release();
                        callback(true);
                        return;
                    }
                    connection.release();
                    callback(false, results);
                });                
            }else{
                Respondents.save(user_id, paper_id, question_id, option,created_at, function(err, results){
                    callback(false, results);
                }) ;
            }

        }) ;
    });
};

Respondents.findAll = function(currPage, pageSize, keywords, callback){
    var currCount = (parseInt(currPage)-1)*pageSize ;
    var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM respondents WHERE 1=1 ";
    if(keywords){
        sql += " AND title LIKE '%"+keywords+"%'" ;
    }
    sql += " ORDER BY created_at DESC LIMIT "+currCount+","+pageSize ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
            var sqlCount = "SELECT FOUND_ROWS() c" ;
            connection.query(sqlCount, function(err, totalCount){
                //console.log(totalCount) ;
                connection.release();
                callback(false, totalCount[0].c, results);
            }) ;
        });
    });    
}
Respondents.findById = function(user_id, paper_id, question_id, callback){
    var sql = "SELECT * FROM respondents WHERE user_id=? AND paper_id = ? AND question_id=?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [user_id, paper_id, question_id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
/**
 * 统计答题历史
 */
Respondents.findPaper = function(user_id, callback){
    var sql = "SELECT p.paper_id,p.title,r.user_id, r.created_at FROM respondents r LEFT JOIN papers p ON r.paper_id=p.paper_id WHERE user_id = ? GROUP BY r.paper_id  ORDER BY r.created_at DESC;";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [user_id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
/**
 * 清空试卷
 */
Respondents.delete = function(user_id,paper_id, callback){
    var sql = "DELETE FROM respondents WHERE user_id=? AND paper_id=?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [user_id,paper_id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
Respondents.deleteByUid = function(uid, callback){
    logger.info(" uid:"+uid) ;
    var sql = "DELETE FROM respondents WHERE user_id=? ";
    var conditions = [uid] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}

module.exports = Respondents ;
