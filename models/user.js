var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var User = function() {};

User.prototype.find = function(id, callback) {
    var sql = "SELECT * FROM users WHERE user_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 用户更新
 */
User.update = function(id, nickname,taobao, alipay, is_fee, power,class_power, settings, exam_time, address, callback) {
    logger.info("nickname:"+nickname+"is_fee："+is_fee+"id:"+id) ;
    settings = JSON.stringify(settings).toString() ;
    var sql = "UPDATE users SET nickname = ? , taobao = ?, alipay = ?, is_fee = ? , power = ?, class_power = ?, settings = ?, exam_time = ?, address = ? WHERE user_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        logger.info(nickname+is_fee+id) ;
        connection.query(sql, [nickname, taobao, alipay, parseInt(is_fee), power, class_power, settings, exam_time, address, parseInt(id)], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
            	logger.info(err) ;
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 用户密码更新
 */
User.updatePwd = function(id,  password, callback) {
	var sql = "UPDATE users SET password = ?  WHERE user_id =?";
	// get a connection from the pool
	db.pool.getConnection(function(err, connection) {
		if (err) {
			connection.release();
			callback(true);
			return;
		}
		// make the query
		connection.query(sql, [password, parseInt(id)], function(err, results) {
			logger.info("sql:"+sql) ;
			if (err) {
				logger.info(err) ;
				callback(true);
				return;
			}
			connection.release();
			callback(false, results);
		});
	});
};
//新用户
User.save = function(mobile,nickname,taobao,alipay,password,exam_time,address,created_at, callback) {
    var sql = "INSERT INTO users(mobile,nickname,taobao,alipay,password,exam_time,address,created_at) VALUES(?,?,?,?,?,?,?,?)";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [mobile,nickname,taobao,alipay,password,exam_time,address,created_at], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results); 
        });
    });
};

User.findAll = function(currPage, pageSize, keywords, callback){
    var currCount = (parseInt(currPage)-1)*pageSize ;
    var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM users WHERE 1=1 ";
    sql += " AND user_id!=1" ;
    if(keywords){
        sql += " AND (mobile LIKE '%"+keywords+"%' OR taobao LIKE '%"+keywords+"%' OR alipay LIKE '%"+keywords+"%')" ;
    }
    sql += " ORDER BY created_at DESC LIMIT "+currCount+","+pageSize ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
            var sqlCount = "SELECT FOUND_ROWS() c" ;
            connection.query(sqlCount, function(err, totalCount){
                //console.log(totalCount) ;
                connection.release();
                callback(false, totalCount[0].c, results);
            }) ;
        });
    });    
}
User.findById = function(id, callback){
    var sql = "SELECT * FROM users WHERE user_id=?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
User.get = function(mobile, callback){
    var sql = "SELECT * FROM users WHERE mobile=?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [mobile], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results[0]);
        });
    });    
}
User.deleteById = function(uid, callback){
    logger.info(" uid:"+uid) ;
    var sql = "DELETE FROM users WHERE user_id=? ";
    var conditions = [uid] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
module.exports = User ;
