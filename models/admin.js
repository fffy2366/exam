/**
 * 管理员	
 */
var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var Admin = function(email,username,password) {
	this.email = email ;
	this.username = username ;
	this.password = password ;	
};

Admin.find = function(username,password, callback) {
    var sql = "SELECT * FROM admin WHERE username =? AND password= ?";
    logger.info("admin find sql:"+sql) ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [username,password], function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};

Admin.findByUsername = function(username, callback) {
    var sql = "SELECT * FROM admin WHERE username =? ";
    logger.info("admin find sql:"+sql) ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [username], function(err, results) {
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
module.exports = Admin ;
