var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var Loginlog = function() {};

Loginlog.prototype.find = function(id, callback) {
    var sql = "SELECT * FROM login_log WHERE user_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 添加
 */
Loginlog.save = function(mobile,ip,login_log, created_at, callback) {
    logger.info("login_log:"+login_log) ;
    var sql = "INSERT INTO login_log (mobile, ip, login_log, created_at) VALUES(?,?,?,?)";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [mobile, ip, login_log, created_at], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 更新
 */
Loginlog.update = function(id, login_log, callback) {
    logger.info("login_log:"+login_log+" id:"+id) ;
    var sql = "UPDATE login_log SET login_log = ?  WHERE mobile =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [login_log, parseInt(id)], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 更新最近一条短信发送状态
 * alter table login_log add column sms_status text after ip;
 */
Loginlog.updateSmsStatus = function(mobile, status, callback) {
    logger.info("mobile:"+mobile+" status:"+status) ;
    var sql = "UPDATE login_log SET sms_status = ?  WHERE mobile =? order by created_at desc limit 1";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [status, mobile], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
Loginlog.findAll = function(callback){
    var sql = "SELECT  * FROM login_log WHERE 1=1 AND is_deleted = 0 ";
   
    sql += " ORDER BY created_at DESC " ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
			connection.release();
			callback(false, results);            
        });
    });    
}
Loginlog.findAllByPage = function(currPage, pageSize, keywords, callback){
    var currCount = (parseInt(currPage)-1)*pageSize ;
    var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM login_log WHERE 1=1 ";
    if(keywords){
        sql += " AND mobile LIKE '%"+keywords+"%'" ;
    }
    sql += " ORDER BY created_at DESC LIMIT "+currCount+","+pageSize ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
            var sqlCount = "SELECT FOUND_ROWS() c" ;
            connection.query(sqlCount, function(err, totalCount){
                //console.log(totalCount) ;
                connection.release();
                callback(false, totalCount[0].c, results);
            }) ;
        });
    });    
}

Loginlog.findByMobile = function(mobile, callback){
    logger.info("mobile: "+mobile) ;
    var sql = "SELECT * FROM login_log WHERE mobile=? ORDER BY id DESC";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [mobile], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
Loginlog.findByMobileLoginlog = function(mobile,login_log, callback){
    logger.info("mobile: "+mobile+" login_log:"+login_log) ;
    //var sql = "SELECT * FROM login_log WHERE mobile=? AND login_log=? AND created_at > date_sub(now(),interval 120 minute) AND is_deleted=0";
    var sql = "SELECT * FROM login_log WHERE mobile=? AND login_log=? AND is_deleted=0";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [mobile,login_log], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
Loginlog.deleteById = function(tid, callback){
    logger.info(" tid:"+tid) ;
    var sql = "UPDATE login_log SET is_deleted = 1 WHERE id=? ";
    var conditions = [tid] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
Loginlog.deleteByMobileLoginlog = function(mobile, mobileLoginlog, callback){
    logger.info("mobile:"+mobile) ;
    logger.info("mobileLoginlog:"+mobileLoginlog) ;
    var sql = "UPDATE login_log SET is_deleted = 1 WHERE mobile=? AND login_log=?";
    var conditions = [mobile, mobileLoginlog] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
Loginlog.findCountByIp = function(ip,callback){
    logger.info("ip: "+ip) ;
    var sql = "SELECT COUNT(id) c FROM `login_log` WHERE ip=? AND to_days(created_at)=to_days(now()) ";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [ip], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
module.exports = Loginlog ;
