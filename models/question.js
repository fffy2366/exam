/**
 * 题库
 */
var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var Question = function() {};

Question.prototype.find = function(id, callback) {
    var sql = "SELECT * FROM questions WHERE question_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};

Question.save = function(area_id, label, title,category_id, num, correct, score, image, sound,titlesound, settings, created_at, callback) {
    logger.info("area_id:"+area_id+"title:"+title+"image:"+image) ;
    settings = JSON.stringify(settings).toString() ;
    var sql = "INSERT INTO questions(area_id,label,title,category_id, num, correct, score, image,sound,titlesound,settings,created_at) VALUES(?,?,?,?,?,?,?,?,?,?,'"+settings+"',?)";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            logger.error(err) ;
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [area_id, label, title,category_id, num, correct, score, image,sound, titlesound, created_at], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.error(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.query("SELECT LAST_INSERT_ID() id",function(err, id){
                if(err){
                    logger.error(err) ;
                    callback(true);
                    return;                    
                }
                connection.release();
                callback(false, id[0]);                
            }) ;
        });
    });
};
Question.update = function(question_id, area_id, label, title, category_id, num, correct, score, image, sound, titlesound, settings, callback) {
    logger.info("question_id:"+question_id) ;
    logger.info("area_id:"+area_id) ;
    logger.info("label:"+label) ;
    logger.info("typeof label:"+typeof label) ;
    logger.info("title:"+title) ;
    logger.info("sound:"+sound) ;
    logger.info("question_id:"+question_id) ;
    settings = JSON.stringify(settings) ;
    logger.info("settings:"+settings) ;
    var sql = "UPDATE questions SET area_id = ? , label = ? , title = ? , category_id = ?, num = ? , correct = ? , score = ? , image = ? , sound = ? , titlesound = ?, settings = '"+settings+"'  WHERE question_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [area_id, label+"", title ,category_id, num, correct, score, image, sound, titlesound, question_id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};

Question.findAll = function(currPage, pageSize, cid, keywords, callback){
    var currCount = (parseInt(currPage)-1)*pageSize ;
    var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM questions WHERE 1=1 AND is_deleted = 0 ";
    var conditions = [] ;
    if(cid){
        sql += " AND category_id = ? " ;
        conditions.push(cid) ;
    }
    if(keywords){
        sql += " AND title LIKE '%"+keywords+"%'" ;
    }
    //sql += " ORDER BY created_at DESC LIMIT "+currCount+","+pageSize ;
    sql += " ORDER BY question_id DESC LIMIT "+currCount+","+pageSize ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
            var sqlCount = "SELECT FOUND_ROWS() c" ;
            connection.query(sqlCount, function(err, totalCount){
                //console.log(totalCount) ;
                connection.release();
                callback(false, totalCount[0].c, results);
            }) ;
        });
    });    
}
Question.findById = function(id, callback){
    var sql = "SELECT * FROM questions WHERE is_deleted = 0 AND Question_id=?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
Question.deleteById = function(qid, callback){
    logger.info(" qid:"+qid) ;
    //var sql = "DELETE FROM questions WHERE question_id=? ";
    var sql = "UPDATE questions SET is_deleted = 1 WHERE question_id=? ";
    var conditions = [qid] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}

module.exports = Question ;
