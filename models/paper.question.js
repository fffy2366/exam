var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var PaperQuestion = function() {};

PaperQuestion.prototype.find = function(id, callback) {
    var sql = "SELECT * FROM paper_question_relation WHERE pager_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 添加
 */
PaperQuestion.save = function(p_id, q_id, created_at, callback) {
    logger.info("p_id:"+p_id+" q_id:"+q_id) ;
    var sql = "INSERT INTO paper_question_relation (paper_id, question_id, created_at) VALUES(?,?,?)";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [p_id, q_id, created_at], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                console.log(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 更新
 */
PaperQuestion.update = function(p_id, q_id, callback) {
    logger.info("p_id:"+p_id+" q_id:"+q_id) ;
    var sql = "UPDATE paper_question_relation SET paper_id = ? ,question_id = ? WHERE id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            console.log("err:"+err) ;
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [title, description, parseInt(id)], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                console.log("err:"+err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
PaperQuestion.findAll = function(callback){
    var sql = "SELECT  * FROM paper_question_relation WHERE 1=1 ";
    
    sql += " ORDER BY created_at DESC " ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
			connection.release();
			callback(false, results);            
        });
    });    
}
PaperQuestion.findAllByPage = function(pid,currPage, pageSize, keywords, callback){
    var currCount = (parseInt(currPage)-1)*pageSize ;
    var sql = "SELECT SQL_CALC_FOUND_ROWS pq.id,pq.*,q.area_id,q.title,q.num,q.correct,q.score,q.image,q.sound,q.titlesound,q.label,q.settings FROM paper_question_relation pq LEFT JOIN questions q ON pq.question_id = q.question_id WHERE 1=1 AND q.is_deleted = 0 ";
    if(pid){
        sql += " AND paper_id = "+pid ;
    }
    if(keywords){
        sql += " AND title LIKE '%"+keywords+"%' " ;
    }
    //sql += " ORDER BY pq.created_at DESC LIMIT "+currCount+","+pageSize ;
    sql += " ORDER BY q.num ASC LIMIT "+currCount+","+pageSize ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            console.log(err) ;
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                console.log("find all err:"+err) ;
                connection.release();
                callback(true);
                return;
            }
            var sqlCount = "SELECT FOUND_ROWS() c" ;
            connection.query(sqlCount, function(err, totalCount){
                //console.log(totalCount) ;
                connection.release();
                callback(false, totalCount[0].c, results);
            }) ;
        });
    });    
}
PaperQuestion.findById = function(p_id, q_id, callback){
    logger.info("p_id:"+p_id+" q_id:"+q_id) ;
    var sql = "SELECT * FROM paper_question_relation WHERE paper_id=? ";
    var conditions = [p_id] ;
    if(q_id!=''){
        sql += " AND question_id=?" ;
        conditions.push(q_id) ;
    }
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            if(connection){
                connection.release();
            }
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
PaperQuestion.deleteById = function(pid, qid, callback){
    logger.info(" qid:"+qid+" pid:"+pid) ;
    var sql = "DELETE FROM paper_question_relation WHERE paper_id=? AND question_id=?";
    var conditions = [pid,qid] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
module.exports = PaperQuestion ;
