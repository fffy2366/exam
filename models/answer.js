/**
 * 题目选项
 */
var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var Answer = function() {};

Answer.prototype.find = function(id, callback) {
    var sql = "SELECT * FROM answers WHERE id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};

Answer.save = function(question_id, title, img, option, created_at, callback) {
    logger.info("question_id:"+question_id+"title:"+title+"img:"+img+"option:"+option+" created_at:"+created_at) ;
    var sql = "INSERT INTO answers(question_id,title,img, options,created_at) VALUES(?,?,?,?,?)";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [question_id, title, img, option, created_at], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info("err"+err) ;
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
Answer.update = function(question_id, title, img, option, created_at, callback) {
    logger.info("question_id:"+question_id+"title"+title+"option:"+option) ;
    var query_sql = "SELECT * FROM answers WHERE question_id =? AND options = ?" ;
    var sql = "UPDATE answers SET title = ? , img = ? WHERE question_id =? AND options = ?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(query_sql, [question_id, option], function(err, answers) {
            if(err){
                logger.error(err) ;
                connection.release();
                callback(true);
                return ;
            }
            //connection.release();
            if (answers.length>0) {
                connection.query(sql, [title, img, question_id, option], function(err, results) {
                    logger.error("sql:"+sql) ;
                    if (err) {
                        connection.release();
                        callback(true);
                        return;
                    }
                    if(connection){
                        connection.release();
                    }
                    callback(false, results);
                });                
            }else{
                Answer.save(question_id, title, img, option, created_at, function(err, results){
                    callback(false, results);
                }) ;
            }

        }) ;
    });
};

Answer.findAll = function(currPage, pageSize, keywords, callback){
    var currCount = (parseInt(currPage)-1)*pageSize ;
    var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM answers WHERE 1=1 ";
    if(keywords){
        sql += " AND title LIKE '%"+keywords+"%'" ;
    }
    sql += " ORDER BY created_at DESC LIMIT "+currCount+","+pageSize ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
            var sqlCount = "SELECT FOUND_ROWS() c" ;
            connection.query(sqlCount, function(err, totalCount){
                //console.log(totalCount) ;
                connection.release();
                callback(false, totalCount[0].c, results);
            }) ;
        });
    });    
}
Answer.findByQuestionId = function(id, callback){
    var sql = "SELECT * FROM answers WHERE question_id=?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
module.exports = Answer ;
