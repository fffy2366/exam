var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var UserLog = function() {};

UserLog.prototype.find = function(id, callback) {
    var sql = "SELECT * FROM login_log WHERE user_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
UserLog.save = function(user_id,mobile,ip,created_at, callback) {
    var sql = "INSERT INTO login_log (user_id,mobile,ip,created_at) VALUES (?,?,?,?)";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [user_id,mobile,ip,created_at], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.error("err:"+err) ;
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
UserLog.update = function(id, nickname, is_fee, callback) {
    logger.info("nickname:"+nickname+"is_fee："+is_fee+"id:"+id) ;
    var sql = "UPDATE login_log SET nickname = ? , is_fee = ? WHERE user_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        logger.info(nickname+is_fee+id) ;
        connection.query(sql, [nickname, parseInt(is_fee), parseInt(id)], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};

UserLog.findAll = function(currPage, pageSize, keywords, callback){
    var currCount = (parseInt(currPage)-1)*pageSize ;
    var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM login_log WHERE 1=1 ";
    if(keywords){
        sql += " AND mobile LIKE '%"+keywords+"%'" ;
    }
    sql += " ORDER BY created_at DESC LIMIT "+currCount+","+pageSize ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
            var sqlCount = "SELECT FOUND_ROWS() c" ;
            connection.query(sqlCount, function(err, totalCount){
                //console.log(totalCount) ;
                connection.release();
                callback(false, totalCount[0].c, results);
            }) ;
        });
    });    
}
UserLog.findById = function(id, callback){
    var sql = "SELECT * FROM login_log WHERE user_id=?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}

UserLog.findOneByMobile = function(mobile, callback){
    var sql = "SELECT ip FROM login_log WHERE mobile=? AND to_days(created_at)=to_days(now()) GROUP BY ip";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [mobile], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
}

UserLog.get = function(mobile, callback){
    var sql = "SELECT * FROM login_log WHERE mobile=?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [mobile], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results[0]);
        });
    });    
}
UserLog.deleteByUid = function(uid, callback){
    logger.info(" uid:"+uid) ;
    var sql = "DELETE FROM login_log WHERE user_id=? ";
    var conditions = [uid] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}

module.exports = UserLog ;
