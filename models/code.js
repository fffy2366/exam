var db = require('./db') ;
var _ = require('underscore');
var logger = require('../Logger') ;

var Code = function() {};

Code.prototype.find = function(id, callback) {
    var sql = "SELECT * FROM code WHERE user_id =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [id], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 添加
 */
Code.save = function(mobile,ip,code, created_at, callback) {
    logger.info("code:"+code) ;
    var sql = "INSERT INTO code (mobile, ip, code, created_at) VALUES(?,?,?,?)";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [mobile, ip, code, created_at], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 更新
 */
Code.update = function(id, code, callback) {
    logger.info("code:"+code+" id:"+id) ;
    var sql = "UPDATE code SET code = ?  WHERE mobile =?";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [code, parseInt(id)], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
/**
 * 更新最近一条短信发送状态
 * alter table code add column sms_status text after ip;
 */
Code.updateSmsStatus = function(mobile, status, callback) {
    logger.info("mobile:"+mobile+" status:"+status) ;
    var sql = "UPDATE code SET sms_status = ?  WHERE mobile =? order by created_at desc limit 1";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [status, mobile], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });
};
Code.findAll = function(callback){
    var sql = "SELECT  * FROM code WHERE 1=1 AND is_deleted = 0 ";
   
    sql += " ORDER BY created_at DESC " ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
			connection.release();
			callback(false, results);            
        });
    });    
}
Code.findAllByPage = function(currPage, pageSize, keywords, callback){
    var currCount = (parseInt(currPage)-1)*pageSize ;
    var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM code WHERE 1=1 ";
    if(keywords){
        sql += " AND mobile LIKE '%"+keywords+"%'" ;
    }
    sql += " ORDER BY created_at DESC LIMIT "+currCount+","+pageSize ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                connection.release();
                callback(true);
                return;
            }
            var sqlCount = "SELECT FOUND_ROWS() c" ;
            connection.query(sqlCount, function(err, totalCount){
                //console.log(totalCount) ;
                connection.release();
                callback(false, totalCount[0].c, results);
            }) ;
        });
    });    
}

Code.findByMobile = function(mobile, callback){
    logger.info("mobile: "+mobile) ;
    var sql = "SELECT * FROM code WHERE mobile=? ORDER BY id DESC";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [mobile], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
Code.findByMobileCode = function(mobile,code, callback){
    logger.info("mobile: "+mobile+" code:"+code) ;
    //var sql = "SELECT * FROM code WHERE mobile=? AND code=? AND created_at > date_sub(now(),interval 120 minute) AND is_deleted=0";
    var sql = "SELECT * FROM code WHERE mobile=? AND code=? AND is_deleted=0";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [mobile,code], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
Code.deleteById = function(tid, callback){
    logger.info(" tid:"+tid) ;
    var sql = "UPDATE code SET is_deleted = 1 WHERE id=? ";
    var conditions = [tid] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
Code.deleteByMobileCode = function(mobile, mobileCode, callback){
    logger.info("mobile:"+mobile) ;
    logger.info("mobileCode:"+mobileCode) ;
    var sql = "UPDATE code SET is_deleted = 1 WHERE mobile=? AND code=?";
    var conditions = [mobile, mobileCode] ;
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, conditions, function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                logger.info(err) ;
                connection.release();
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
Code.findCountByIp = function(ip,callback){
    logger.info("ip: "+ip) ;
    var sql = "SELECT COUNT(id) c FROM `code` WHERE ip=? AND to_days(created_at)=to_days(now()) ";
    // get a connection from the pool
    db.pool.getConnection(function(err, connection) {
        if (err) {
            connection.release();
            callback(true);
            return;
        }
        // make the query
        connection.query(sql, [ip], function(err, results) {
            logger.info("sql:"+sql) ;
            if (err) {
                callback(true);
                return;
            }
            connection.release();
            callback(false, results);
        });
    });    
}
module.exports = Code ;
