/**
 * 字符串处理
 * @type {exports}
 */
var fs = require('fs') ; 
var path = require('path') ; 
var StringUtils = {};
//异步的实现
StringUtils.mkdir = function(dirpath, mode, callback) {
    if(arguments.length === 2) {
        callback = mode;
        mode = 0777;
    }
	console.log("dirpath:"+dirpath) ;
    fs.exists(dirpath, function(exists) {
        if(exists) {
            callback(null);
        } else {
            StringUtils.mkdir(path.dirname(dirpath), mode, function(err) {
                // console.log('>>', dirpath)
                if(err) return callback(err);
                fs.mkdir(dirpath, mode, callback);
            });
        }
    });
};
//同步的实现
StringUtils.mkdirSync = function(dirpath, mode) {
    dirpath.split('\/').reduce(function(pre, cur) {
        var p = path.resolve(pre, cur);
        if(!fs.existsSync(p)) fs.mkdirSync(p, mode || 0755);
        return p;
    }, __dirname);
};
StringUtils.getClientIp = function(req) {
    var ipAddress;
    var forwardedIpsStr = req.header('x-forwarded-for'); 
    if (forwardedIpsStr) {
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        ipAddress = req.connection.remoteAddress;
    }
    return ipAddress;
};
StringUtils.removeFile = function(file,callback){
	fs.exists(file, function(exists) {
        if(exists) {
        	fs.unlinkSync(file);
            callback("success");
        }else{
        	callback("nofile") ;
        }
	});
}
module.exports = StringUtils;