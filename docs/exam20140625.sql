-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: exam
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `username` varchar(32) DEFAULT NULL COMMENT '姓名',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'fengxuting@qq.com','frank','123456','2014-04-07 19:09:51','2014-04-07 11:09:54',0);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `question_id` int(11) DEFAULT NULL COMMENT 'ID',
  `title` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `options` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_4` (`question_id`),
  CONSTRAINT `FK_Reference_4` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='答案';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,2,'a','808206c0-cc4d-11e3-b923-7d3596959a65.jpg','A','2014-04-25 15:46:58','2014-04-25 07:46:58',0),(2,2,'','','B','2014-04-25 15:46:58','2014-04-25 07:46:58',0),(3,2,'','','C','2014-04-25 15:46:58','2014-04-25 07:46:58',0),(4,2,'','','D','2014-04-25 15:46:58','2014-04-25 07:46:58',0),(5,1,'','','A','2014-04-25 15:49:15','2014-04-25 07:49:15',0),(6,1,'','','B','2014-04-25 15:49:15','2014-04-25 07:49:15',0),(7,1,'','','C','2014-04-25 15:49:15','2014-04-25 07:49:15',0),(8,1,'','','D','2014-04-25 15:49:15','2014-04-25 07:49:15',0),(9,3,'选项A','','A','2014-05-11 21:47:50','2014-05-16 08:52:45',0),(10,3,'选项b','64b9e200-d90e-11e3-b21b-b9d95c4f7cd3.jpg','B','2014-05-11 21:47:50','2014-05-16 07:05:38',0),(11,3,'','66bda140-d90e-11e3-b21b-b9d95c4f7cd3.jpg','C','2014-05-11 21:47:50','2014-05-11 13:47:50',0),(12,3,'','68d73270-d90e-11e3-b21b-b9d95c4f7cd3.jpg','D','2014-05-11 21:47:50','2014-05-11 13:47:50',0),(13,4,'a','','A','2014-05-16 15:30:52','2014-05-16 07:30:52',0),(14,4,'b','','B','2014-05-16 15:30:52','2014-05-16 07:30:52',0),(15,4,'c','','C','2014-05-16 15:30:52','2014-05-16 07:30:52',0),(16,4,'d','','D','2014-05-16 15:30:52','2014-05-16 07:30:52',0),(17,5,'琵琶','','A','2014-06-20 18:07:04','2014-06-20 10:07:04',0),(18,5,'手风琴','','B','2014-06-20 18:07:04','2014-06-20 10:07:04',0),(19,5,'口风琴','','C','2014-06-20 18:07:04','2014-06-20 10:07:04',0),(20,5,'笛子','','D','2014-06-20 18:07:04','2014-06-20 10:07:04',0),(21,6,'','f5f08170-f862-11e3-a034-85f870ab6f69.jpg','A','2014-06-20 18:10:23','2014-06-20 10:10:23',0),(22,6,'','07697790-f863-11e3-a034-85f870ab6f69.jpg','B','2014-06-20 18:10:23','2014-06-20 10:10:23',0),(23,6,'','0cfaf170-f863-11e3-a034-85f870ab6f69.jpg','C','2014-06-20 18:10:23','2014-06-20 10:10:23',0),(24,6,'','16d8bb00-f863-11e3-a034-85f870ab6f69.gif','D','2014-06-20 18:10:23','2014-06-20 10:10:23',0),(25,7,'笛子','','A','2014-06-20 18:22:55','2014-06-20 10:22:55',0),(26,7,'唢呐','','B','2014-06-20 18:22:55','2014-06-20 10:22:55',0),(27,7,'埙','','C','2014-06-20 18:22:55','2014-06-20 10:22:55',0),(28,7,'二胡','','D','2014-06-20 18:22:55','2014-06-20 10:22:55',0);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT '类别名称',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='地区：全国or北京';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,'全国',NULL,'2014-04-12 14:45:35',0),(2,'北京',NULL,'2014-04-12 14:45:42',0);
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classification`
--

DROP TABLE IF EXISTS `classification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classification` (
  `classification_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`classification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classification`
--

LOCK TABLES `classification` WRITE;
/*!40000 ALTER TABLE `classification` DISABLE KEYS */;
INSERT INTO `classification` VALUES (1,'分类训练1','2014-05-10 23:44:14','2014-05-10 15:44:14',0),(2,'分类训练二','2014-05-10 23:44:29','2014-05-10 15:44:29',0),(3,'分类训练三','2014-05-10 23:44:35','2014-05-10 15:44:35',0),(4,'分类训练四','2014-05-10 23:44:44','2014-05-10 15:44:44',0);
/*!40000 ALTER TABLE `classification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `code`
--

DROP TABLE IF EXISTS `code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `code` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `mobile` varchar(32) DEFAULT NULL COMMENT '手机号',
  `code` varchar(32) DEFAULT NULL COMMENT '验证码',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='手机验证码';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `code`
--

LOCK TABLES `code` WRITE;
/*!40000 ALTER TABLE `code` DISABLE KEYS */;
INSERT INTO `code` VALUES (1,'13001047235','5264','2014-05-19 17:50:38','2014-05-19 10:33:22',0),(2,'13001047235','3555','2014-05-19 21:03:01','2014-05-19 13:03:01',0),(3,'18910737107','9770','2014-05-21 12:48:45','2014-05-21 04:48:45',0),(4,'13001047235','8190','2014-05-21 13:17:49','2014-05-21 05:17:49',0),(5,'13261044588','4704','2014-05-21 13:20:39','2014-05-21 05:20:39',0),(6,'13001047235','4358','2014-05-21 13:23:57','2014-05-21 05:23:57',0),(7,'13001047235','6036','2014-05-21 13:28:25','2014-05-21 05:28:25',0),(8,'13001047235','7568','2014-05-21 13:28:38','2014-05-21 05:28:38',0),(9,'13621370125','7987','2014-05-21 13:50:41','2014-05-21 05:50:41',0),(10,'13621370125','6657','2014-05-21 14:11:32','2014-05-21 06:45:18',1),(11,'13001047235','8237','2014-05-21 14:34:55','2014-05-21 06:34:55',0),(12,'18910737107','2188','2014-05-21 17:51:27','2014-05-21 09:51:45',1),(13,'18910737107','6615','2014-06-23 11:45:49','2014-06-23 03:45:49',0),(14,'13522019240','2384','2014-06-23 11:47:07','2014-06-23 03:47:38',1),(15,'13522019240','6068','2014-06-23 11:48:03','2014-06-23 03:48:03',0),(16,'13261044588','5619','2014-06-23 11:51:02','2014-06-23 03:51:02',0),(17,'18610042069','2526','2014-06-23 11:52:24','2014-06-23 03:54:20',1),(18,'18610042069','2714','2014-06-23 11:58:59','2014-06-23 03:59:25',1),(19,'18910737107','3934','2014-06-23 12:17:58','2014-06-23 04:18:21',1),(20,'18910737107','0758','2014-06-25 10:22:34','2014-06-25 02:23:46',1),(21,'18910737107','8756','2014-06-25 11:27:23','2014-06-25 03:27:39',1),(22,'18910737107','7725','2014-06-25 11:43:46','2014-06-25 03:44:10',1);
/*!40000 ALTER TABLE `code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graded`
--

DROP TABLE IF EXISTS `graded`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graded` (
  `graded_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`graded_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graded`
--

LOCK TABLES `graded` WRITE;
/*!40000 ALTER TABLE `graded` DISABLE KEYS */;
INSERT INTO `graded` VALUES (1,'北京初级','2014-05-10 23:36:09','2014-05-10 15:41:40',0),(2,'北京中级','2014-05-10 23:41:54','2014-05-10 15:41:54',0),(3,'全国初级','2014-05-10 23:42:26','2014-05-10 15:42:26',0),(4,'全国中级','2014-05-10 23:42:43','2014-05-10 15:42:43',0),(5,'初级视唱','2014-05-10 23:42:53','2014-05-10 15:42:53',0),(6,'中级视唱','2014-05-10 23:43:06','2014-05-10 15:43:06',0),(7,'初级模唱','2014-05-10 23:43:19','2014-05-10 15:43:19',0),(8,'中级模唱','2014-05-10 23:43:32','2014-05-10 15:43:32',0);
/*!40000 ALTER TABLE `graded` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_log`
--

DROP TABLE IF EXISTS `login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `mobile` varchar(32) DEFAULT NULL COMMENT '手机号',
  `ip` varchar(32) DEFAULT NULL COMMENT 'IP',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_1` (`user_id`),
  CONSTRAINT `FK_Reference_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='登录日志，一个账号24小时内只运行一个IP登录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_log`
--

LOCK TABLES `login_log` WRITE;
/*!40000 ALTER TABLE `login_log` DISABLE KEYS */;
INSERT INTO `login_log` VALUES (1,2,'13001047235','127.0.0.1','2014-05-19 17:46:41','2014-05-19 09:46:41',0),(2,2,'13001047235','182.18.6.106','2014-05-21 13:23:07','2014-05-21 05:23:07',0),(3,9,'13261044588','221.218.15.217','2014-05-21 13:23:26','2014-05-21 05:23:26',0),(4,2,'13001047235','182.18.6.106','2014-05-21 14:35:14','2014-05-21 06:35:14',0),(5,2,'13001047235','182.18.6.106','2014-05-21 14:38:57','2014-05-21 06:38:57',0),(6,1,'13621370125','182.18.6.106','2014-05-21 14:45:18','2014-05-21 06:45:18',0),(7,8,'18910737107','124.127.71.154','2014-05-21 17:51:45','2014-05-21 09:51:45',0),(8,11,'18610042069','111.204.255.15','2014-06-23 11:59:25','2014-06-23 03:59:25',0),(9,8,'18910737107','222.131.125.28','2014-06-23 12:18:21','2014-06-23 04:18:21',0),(10,8,'18910737107','124.127.64.53','2014-06-25 10:23:46','2014-06-25 02:23:46',0),(11,8,'18910737107','124.127.64.53','2014-06-25 11:27:39','2014-06-25 03:27:39',0),(12,8,'18910737107','124.127.64.53','2014-06-25 11:44:10','2014-06-25 03:44:10',0);
/*!40000 ALTER TABLE `login_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `options` (
  `option_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'option_id',
  `option_name` varchar(64) DEFAULT NULL COMMENT 'option_name',
  `option_value` longtext COMMENT 'option_value',
  `autoload` varchar(20) DEFAULT 'yes' COMMENT 'autoload',
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `options`
--

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
INSERT INTO `options` VALUES (1,'siteurl',NULL,'yes'),(2,'sitename','第一音乐在线网','yes'),(3,'sitedesciption','第一音乐在线网','yes');
/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paper_question_relation`
--

DROP TABLE IF EXISTS `paper_question_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paper_question_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `paper_id` int(11) DEFAULT NULL COMMENT 'ID',
  `question_id` int(11) DEFAULT NULL COMMENT 'ID',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_2` (`paper_id`),
  KEY `FK_Reference_3` (`question_id`),
  CONSTRAINT `FK_Reference_2` FOREIGN KEY (`paper_id`) REFERENCES `papers` (`paper_id`),
  CONSTRAINT `FK_Reference_3` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='试卷题目关系';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paper_question_relation`
--

LOCK TABLES `paper_question_relation` WRITE;
/*!40000 ALTER TABLE `paper_question_relation` DISABLE KEYS */;
INSERT INTO `paper_question_relation` VALUES (6,1,1,'2014-04-25 22:52:23','2014-04-25 14:52:23',0),(7,1,3,'2014-05-11 21:48:44','2014-05-11 13:48:44',0),(8,1,2,'2014-05-11 21:51:07','2014-05-11 13:51:07',0),(9,14,2,'2014-05-15 11:34:01','2014-05-15 03:34:01',0),(10,14,1,'2014-05-15 11:34:01','2014-05-15 03:34:01',0),(11,14,3,'2014-05-15 11:34:01','2014-05-15 03:34:01',0),(12,14,7,'2014-06-23 12:07:06','2014-06-23 04:07:06',0),(13,14,6,'2014-06-23 12:07:06','2014-06-23 04:07:06',0),(14,14,5,'2014-06-23 12:07:06','2014-06-23 04:07:06',0),(15,14,4,'2014-06-23 12:07:06','2014-06-23 04:07:06',0);
/*!40000 ALTER TABLE `paper_question_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `papers`
--

DROP TABLE IF EXISTS `papers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `papers` (
  `paper_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `graded_id` int(11) DEFAULT NULL,
  `classification_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '试卷名称',
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`paper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='试卷';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `papers`
--

LOCK TABLES `papers` WRITE;
/*!40000 ALTER TABLE `papers` DISABLE KEYS */;
INSERT INTO `papers` VALUES (1,4,4,'在线测试题库一',' 试卷1      ','2014-04-24 22:58:15','2014-05-11 02:53:31',0),(2,1,1,'在线测试题库二',' ','2014-05-11 10:53:45','2014-05-11 02:53:45',0),(3,1,1,'在线测试题库三',' ','2014-05-11 10:53:58','2014-05-11 02:53:58',0),(4,1,1,'在线测试题库四',' ','2014-05-11 10:54:07','2014-05-11 02:54:07',0),(5,0,3,'在线测试题库五',' ','2014-05-11 10:54:14','2014-05-15 05:21:53',0),(6,0,1,'在线测试题库六',' ','2014-05-11 10:54:21','2014-05-15 05:21:46',0),(7,0,2,'在线测试题库七','  ','2014-05-11 10:54:30','2014-05-21 10:47:03',0),(8,1,0,'在线测试题库八',' ','2014-05-11 10:54:38','2014-05-15 05:21:45',0),(9,1,1,'在线测试题库九',' ','2014-05-11 10:54:47','2014-05-11 02:54:47',0),(10,1,0,'在线测试题库十',' ','2014-05-11 10:54:56','2014-05-15 05:22:01',0),(11,0,4,'在线测试题库十一','  ','2014-05-11 10:55:04','2014-05-15 05:20:57',0),(12,1,0,'在线测试题库十二','  ','2014-05-11 10:55:17','2014-05-15 05:20:47',0),(13,1,0,'在线测试题库十三','  ','2014-05-11 11:04:35','2014-05-15 05:20:05',0),(14,1,0,'在线测试题库十四','     ','2014-05-11 11:05:27','2014-05-21 10:46:51',0);
/*!40000 ALTER TABLE `papers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_tag_relation`
--

DROP TABLE IF EXISTS `question_tag_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_tag_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `question_id` int(11) DEFAULT NULL COMMENT 'ID',
  `tag_id` int(11) DEFAULT NULL COMMENT 'ID',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_10` (`tag_id`),
  KEY `FK_Reference_9` (`question_id`),
  CONSTRAINT `FK_Reference_10` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`),
  CONSTRAINT `FK_Reference_9` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='问题-标签关系表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_tag_relation`
--

LOCK TABLES `question_tag_relation` WRITE;
/*!40000 ALTER TABLE `question_tag_relation` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_tag_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `area_id` int(11) DEFAULT NULL COMMENT 'ID',
  `title` varchar(255) DEFAULT NULL COMMENT '题目',
  `correct` varchar(32) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL COMMENT '图片',
  `sound` varchar(255) DEFAULT NULL COMMENT '声音',
  `settings` text,
  `label` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`question_id`),
  KEY `FK_Reference_8` (`area_id`),
  CONSTRAINT `FK_Reference_8` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='问题';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,1,'问题1','B','2ff2ba10-cbc1-11e3-bf98-d9b713b1dfc1.jpg','null',NULL,'1','2014-04-12 22:48:18','2014-05-14 09:26:31',0),(2,NULL,'问题2','C','54422a50-cc4c-11e3-b923-7d3596959a65.jpg','55aa9e80-cc4d-11e3-b923-7d3596959a65.mp3','{\"autoplay\":\"no\",\"playcount\":\"1\"}','undefined','2014-04-12 22:48:18','2014-06-20 02:53:54',0),(3,NULL,'下面选项中，与播放音乐一致的是：','B','a77a4f10-e090-11e3-8e02-cd5d5b874ad5.jpg','7000ab70-e0a3-11e3-b369-3d93bfc722ca.mp3',NULL,'1','2014-05-11 21:47:50','2014-05-21 04:50:34',0),(4,NULL,'测试','A','','','{\"autoplay\":\"no\",\"playcount\":\"1\"}','undefined','2014-05-16 15:30:52','2014-06-20 02:56:17',0),(5,NULL,'21.图片中的乐器是（  ）。','B','6f9e36d0-f862-11e3-a034-85f870ab6f69.jpg','8d5072b0-f862-11e3-a034-85f870ab6f69.mp3','{\"autoplay\":\"no\",\"playcount\":\"1\"}',NULL,'2014-06-20 18:07:04','2014-06-20 10:07:04',0),(6,NULL,'25下列图片中与其他三件乐器类别不同的是（  ）。','C','','ceb0bee0-f862-11e3-a034-85f870ab6f69.mp3','{\"autoplay\":\"yes\",\"playcount\":\"1\"}',NULL,'2014-06-20 18:10:23','2014-06-20 10:10:23',0),(7,NULL,'28.演奏这段乐曲所使用的主要乐器是（  ）。','A','','08803700-fa8d-11e3-a034-85f870ab6f69.mp3','{\"autoplay\":\"yes\",\"playcount\":\"2\"}','undefined','2014-06-20 18:22:55','2014-06-23 04:15:42',0);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `respondents`
--

DROP TABLE IF EXISTS `respondents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `respondents` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `paper_id` int(11) DEFAULT NULL COMMENT 'ID',
  `question_id` int(11) DEFAULT NULL COMMENT 'ID',
  `answer` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_5` (`user_id`),
  KEY `FK_Reference_6` (`paper_id`),
  KEY `FK_Reference_7` (`question_id`),
  CONSTRAINT `FK_Reference_5` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `FK_Reference_6` FOREIGN KEY (`paper_id`) REFERENCES `papers` (`paper_id`),
  CONSTRAINT `FK_Reference_7` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='学生答卷';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `respondents`
--

LOCK TABLES `respondents` WRITE;
/*!40000 ALTER TABLE `respondents` DISABLE KEYS */;
INSERT INTO `respondents` VALUES (4,1,14,1,'B','2014-05-15 11:34:52','2014-05-15 03:34:52',0),(5,1,14,2,'B','2014-05-15 11:34:54','2014-05-15 03:34:54',0),(6,1,14,3,'B','2014-05-15 11:34:56','2014-05-15 03:34:56',0),(7,1,1,1,'A','2014-05-16 14:45:34','2014-05-16 06:45:34',0),(8,1,1,3,'B','2014-05-16 17:16:44','2014-05-16 09:16:44',0),(9,2,1,2,'B','2014-05-17 18:38:54','2014-05-17 10:38:54',0),(10,2,1,3,'B','2014-05-17 18:38:58','2014-05-17 10:39:12',0),(11,2,1,1,'C','2014-05-17 18:39:02','2014-05-17 10:39:02',0),(12,8,14,7,'A','2014-06-23 12:18:53','2014-06-23 04:18:53',0);
/*!40000 ALTER TABLE `respondents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT '类别名称',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='题目标签';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES (1,'标签','2014-04-24 23:00:05','2014-05-10 15:41:30',0),(2,'标签2','2014-04-24 23:00:13','2014-04-26 04:11:55',1);
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `nickname` varchar(32) DEFAULT NULL,
  `mobile` varchar(32) DEFAULT NULL COMMENT '手机',
  `taobao` varchar(255) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `sourse` varchar(32) DEFAULT NULL COMMENT '来源',
  `is_fee` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'frank','13621370125',NULL,'4QrcOUm6Wau+VuBX8g+IPg==',NULL,1,'2014-04-06 16:05:02','2014-05-21 06:45:14',0),(2,'test2','13001047235',NULL,'4QrcOUm6Wau+VuBX8g+IPg==',NULL,1,'2014-04-07 22:54:25','2014-05-17 03:42:28',0),(3,'frank2','1300104723','frankly88','4QrcOUm6Wau+VuBX8g+IPg==',NULL,0,'2014-05-19 18:34:00','2014-05-19 10:34:00',0),(4,'f','13001047231','f','4QrcOUm6Wau+VuBX8g+IPg==',NULL,0,'2014-05-19 18:41:33','2014-05-19 10:47:11',0),(5,'f','13001047232','f','4QrcOUm6Wau+VuBX8g+IPg==',NULL,0,'2014-05-19 18:42:47','2014-05-19 10:47:13',0),(6,'f','13001047234','f','4QrcOUm6Wau+VuBX8g+IPg==',NULL,0,'2014-05-19 18:42:55','2014-05-19 10:42:55',0),(7,'f','13001047233','f','4QrcOUm6Wau+VuBX8g+IPg==',NULL,0,'2014-05-19 18:45:49','2014-05-19 10:45:49',0),(8,'就爱矿泉水','18910737107','火眼心经','sSo6bXDHvy1bBJ/NZYw29w==',NULL,1,'2014-05-21 12:49:33','2014-05-21 04:52:56',0),(9,'kuangquanshui','13261044588','non','sSo6bXDHvy1bBJ/NZYw29w==',NULL,1,'2014-05-21 13:21:30','2014-05-21 05:22:15',0),(10,'爱音乐','13522019240','爱音乐','sSo6bXDHvy1bBJ/NZYw29w==',NULL,1,'2014-06-23 11:47:38','2014-06-23 03:57:57',0),(11,'甄老师','18610042069','','4QrcOUm6Wau+VuBX8g+IPg==',NULL,1,'2014-06-23 11:54:20','2014-06-23 03:57:45',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-25 22:21:32
