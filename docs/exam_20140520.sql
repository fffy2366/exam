/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : exam

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2014-05-20 20:00:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `username` varchar(32) DEFAULT NULL COMMENT '姓名',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'fengxuting@qq.com', 'frank', '123456', '2014-04-07 19:09:51', '2014-04-07 19:09:54', '0');

-- ----------------------------
-- Table structure for `answers`
-- ----------------------------
DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `question_id` int(11) DEFAULT NULL COMMENT 'ID',
  `title` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `options` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_4` (`question_id`),
  CONSTRAINT `FK_Reference_4` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='答案';

-- ----------------------------
-- Records of answers
-- ----------------------------
INSERT INTO `answers` VALUES ('1', '2', 'a', '808206c0-cc4d-11e3-b923-7d3596959a65.jpg', 'A', '2014-04-25 15:46:58', '2014-04-25 15:46:58', '0');
INSERT INTO `answers` VALUES ('2', '2', '', '', 'B', '2014-04-25 15:46:58', '2014-04-25 15:46:58', '0');
INSERT INTO `answers` VALUES ('3', '2', '', '', 'C', '2014-04-25 15:46:58', '2014-04-25 15:46:58', '0');
INSERT INTO `answers` VALUES ('4', '2', '', '', 'D', '2014-04-25 15:46:58', '2014-04-25 15:46:58', '0');
INSERT INTO `answers` VALUES ('5', '1', '', '', 'A', '2014-04-25 15:49:15', '2014-04-25 15:49:15', '0');
INSERT INTO `answers` VALUES ('6', '1', '', '', 'B', '2014-04-25 15:49:15', '2014-04-25 15:49:15', '0');
INSERT INTO `answers` VALUES ('7', '1', '', '', 'C', '2014-04-25 15:49:15', '2014-04-25 15:49:15', '0');
INSERT INTO `answers` VALUES ('8', '1', '', '', 'D', '2014-04-25 15:49:15', '2014-04-25 15:49:15', '0');
INSERT INTO `answers` VALUES ('9', '3', '选项A', '', 'A', '2014-05-11 21:47:50', '2014-05-16 16:52:45', '0');
INSERT INTO `answers` VALUES ('10', '3', '选项b', '64b9e200-d90e-11e3-b21b-b9d95c4f7cd3.jpg', 'B', '2014-05-11 21:47:50', '2014-05-16 15:05:38', '0');
INSERT INTO `answers` VALUES ('11', '3', '', '66bda140-d90e-11e3-b21b-b9d95c4f7cd3.jpg', 'C', '2014-05-11 21:47:50', '2014-05-11 21:47:50', '0');
INSERT INTO `answers` VALUES ('12', '3', '', '68d73270-d90e-11e3-b21b-b9d95c4f7cd3.jpg', 'D', '2014-05-11 21:47:50', '2014-05-11 21:47:50', '0');
INSERT INTO `answers` VALUES ('13', '4', 'a', '', 'A', '2014-05-16 15:30:52', '2014-05-16 15:30:52', '0');
INSERT INTO `answers` VALUES ('14', '4', 'b', '', 'B', '2014-05-16 15:30:52', '2014-05-16 15:30:52', '0');
INSERT INTO `answers` VALUES ('15', '4', 'c', '', 'C', '2014-05-16 15:30:52', '2014-05-16 15:30:52', '0');
INSERT INTO `answers` VALUES ('16', '4', 'd', '', 'D', '2014-05-16 15:30:52', '2014-05-16 15:30:52', '0');

-- ----------------------------
-- Table structure for `area`
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT '类别名称',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='地区：全国or北京';

-- ----------------------------
-- Records of area
-- ----------------------------
INSERT INTO `area` VALUES ('1', '全国', null, '2014-04-12 22:45:35', '0');
INSERT INTO `area` VALUES ('2', '北京', null, '2014-04-12 22:45:42', '0');

-- ----------------------------
-- Table structure for `classification`
-- ----------------------------
DROP TABLE IF EXISTS `classification`;
CREATE TABLE `classification` (
  `classification_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`classification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of classification
-- ----------------------------
INSERT INTO `classification` VALUES ('1', '分类训练1', '2014-05-10 23:44:14', '2014-05-10 23:44:14', '0');
INSERT INTO `classification` VALUES ('2', '分类训练二', '2014-05-10 23:44:29', '2014-05-10 23:44:29', '0');
INSERT INTO `classification` VALUES ('3', '分类训练三', '2014-05-10 23:44:35', '2014-05-10 23:44:35', '0');
INSERT INTO `classification` VALUES ('4', '分类训练四', '2014-05-10 23:44:44', '2014-05-10 23:44:44', '0');

-- ----------------------------
-- Table structure for `code`
-- ----------------------------
DROP TABLE IF EXISTS `code`;
CREATE TABLE `code` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `mobile` varchar(32) DEFAULT NULL COMMENT '手机号',
  `code` varchar(32) DEFAULT NULL COMMENT '验证码',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='手机验证码';

-- ----------------------------
-- Records of code
-- ----------------------------
INSERT INTO `code` VALUES ('1', '13001047235', '5264', '2014-05-19 17:50:38', '2014-05-19 18:33:22', '0');
INSERT INTO `code` VALUES ('2', '13001047235', '3555', '2014-05-19 21:03:01', '2014-05-19 21:03:01', '0');

-- ----------------------------
-- Table structure for `graded`
-- ----------------------------
DROP TABLE IF EXISTS `graded`;
CREATE TABLE `graded` (
  `graded_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`graded_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of graded
-- ----------------------------
INSERT INTO `graded` VALUES ('1', '北京初级', '2014-05-10 23:36:09', '2014-05-10 23:41:40', '0');
INSERT INTO `graded` VALUES ('2', '北京中级', '2014-05-10 23:41:54', '2014-05-10 23:41:54', '0');
INSERT INTO `graded` VALUES ('3', '全国初级', '2014-05-10 23:42:26', '2014-05-10 23:42:26', '0');
INSERT INTO `graded` VALUES ('4', '全国中级', '2014-05-10 23:42:43', '2014-05-10 23:42:43', '0');
INSERT INTO `graded` VALUES ('5', '初级视唱', '2014-05-10 23:42:53', '2014-05-10 23:42:53', '0');
INSERT INTO `graded` VALUES ('6', '中级视唱', '2014-05-10 23:43:06', '2014-05-10 23:43:06', '0');
INSERT INTO `graded` VALUES ('7', '初级模唱', '2014-05-10 23:43:19', '2014-05-10 23:43:19', '0');
INSERT INTO `graded` VALUES ('8', '中级模唱', '2014-05-10 23:43:32', '2014-05-10 23:43:32', '0');

-- ----------------------------
-- Table structure for `login_log`
-- ----------------------------
DROP TABLE IF EXISTS `login_log`;
CREATE TABLE `login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `mobile` varchar(32) DEFAULT NULL COMMENT '手机号',
  `ip` varchar(32) DEFAULT NULL COMMENT 'IP',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_1` (`user_id`),
  CONSTRAINT `FK_Reference_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='登录日志，一个账号24小时内只运行一个IP登录';

-- ----------------------------
-- Records of login_log
-- ----------------------------
INSERT INTO `login_log` VALUES ('1', '2', '13001047235', '127.0.0.1', '2014-05-19 17:46:41', '2014-05-19 17:46:41', '0');

-- ----------------------------
-- Table structure for `options`
-- ----------------------------
DROP TABLE IF EXISTS `options`;
CREATE TABLE `options` (
  `option_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'option_id',
  `option_name` varchar(64) DEFAULT NULL COMMENT 'option_name',
  `option_value` longtext COMMENT 'option_value',
  `autoload` varchar(20) DEFAULT 'yes' COMMENT 'autoload',
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of options
-- ----------------------------
INSERT INTO `options` VALUES ('1', 'siteurl', null, 'yes');
INSERT INTO `options` VALUES ('2', 'sitename', '音乐等级考试', 'yes');
INSERT INTO `options` VALUES ('3', 'sitedesciption', '音乐等级考试', 'yes');

-- ----------------------------
-- Table structure for `papers`
-- ----------------------------
DROP TABLE IF EXISTS `papers`;
CREATE TABLE `papers` (
  `paper_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `graded_id` int(11) DEFAULT NULL,
  `classification_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '试卷名称',
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`paper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='试卷';

-- ----------------------------
-- Records of papers
-- ----------------------------
INSERT INTO `papers` VALUES ('1', '4', '4', '在线测试题库一', ' 试卷1      ', '2014-04-24 22:58:15', '2014-05-11 10:53:31', '0');
INSERT INTO `papers` VALUES ('2', '1', '1', '在线测试题库二', ' ', '2014-05-11 10:53:45', '2014-05-11 10:53:45', '0');
INSERT INTO `papers` VALUES ('3', '1', '1', '在线测试题库三', ' ', '2014-05-11 10:53:58', '2014-05-11 10:53:58', '0');
INSERT INTO `papers` VALUES ('4', '1', '1', '在线测试题库四', ' ', '2014-05-11 10:54:07', '2014-05-11 10:54:07', '0');
INSERT INTO `papers` VALUES ('5', '0', '3', '在线测试题库五', ' ', '2014-05-11 10:54:14', '2014-05-15 13:21:53', '0');
INSERT INTO `papers` VALUES ('6', '0', '1', '在线测试题库六', ' ', '2014-05-11 10:54:21', '2014-05-15 13:21:46', '0');
INSERT INTO `papers` VALUES ('7', '1', '1', '在线测试题库七', ' ', '2014-05-11 10:54:30', '2014-05-11 10:54:30', '0');
INSERT INTO `papers` VALUES ('8', '1', '0', '在线测试题库八', ' ', '2014-05-11 10:54:38', '2014-05-15 13:21:45', '0');
INSERT INTO `papers` VALUES ('9', '1', '1', '在线测试题库九', ' ', '2014-05-11 10:54:47', '2014-05-11 10:54:47', '0');
INSERT INTO `papers` VALUES ('10', '1', '0', '在线测试题库十', ' ', '2014-05-11 10:54:56', '2014-05-15 13:22:01', '0');
INSERT INTO `papers` VALUES ('11', '0', '4', '在线测试题库十一', '  ', '2014-05-11 10:55:04', '2014-05-15 13:20:57', '0');
INSERT INTO `papers` VALUES ('12', '1', '0', '在线测试题库十二', '  ', '2014-05-11 10:55:17', '2014-05-15 13:20:47', '0');
INSERT INTO `papers` VALUES ('13', '1', '0', '在线测试题库十三', '  ', '2014-05-11 11:04:35', '2014-05-15 13:20:05', '0');
INSERT INTO `papers` VALUES ('14', '0', '2', '在线测试题库十四', '    ', '2014-05-11 11:05:27', '2014-05-15 13:19:56', '0');

-- ----------------------------
-- Table structure for `paper_question_relation`
-- ----------------------------
DROP TABLE IF EXISTS `paper_question_relation`;
CREATE TABLE `paper_question_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `paper_id` int(11) DEFAULT NULL COMMENT 'ID',
  `question_id` int(11) DEFAULT NULL COMMENT 'ID',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_2` (`paper_id`),
  KEY `FK_Reference_3` (`question_id`),
  CONSTRAINT `FK_Reference_2` FOREIGN KEY (`paper_id`) REFERENCES `papers` (`paper_id`),
  CONSTRAINT `FK_Reference_3` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='试卷题目关系';

-- ----------------------------
-- Records of paper_question_relation
-- ----------------------------
INSERT INTO `paper_question_relation` VALUES ('6', '1', '1', '2014-04-25 22:52:23', '2014-04-25 22:52:23', '0');
INSERT INTO `paper_question_relation` VALUES ('7', '1', '3', '2014-05-11 21:48:44', '2014-05-11 21:48:44', '0');
INSERT INTO `paper_question_relation` VALUES ('8', '1', '2', '2014-05-11 21:51:07', '2014-05-11 21:51:07', '0');
INSERT INTO `paper_question_relation` VALUES ('9', '14', '2', '2014-05-15 11:34:01', '2014-05-15 11:34:01', '0');
INSERT INTO `paper_question_relation` VALUES ('10', '14', '1', '2014-05-15 11:34:01', '2014-05-15 11:34:01', '0');
INSERT INTO `paper_question_relation` VALUES ('11', '14', '3', '2014-05-15 11:34:01', '2014-05-15 11:34:01', '0');

-- ----------------------------
-- Table structure for `questions`
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `area_id` int(11) DEFAULT NULL COMMENT 'ID',
  `title` varchar(255) DEFAULT NULL COMMENT '题目',
  `correct` varchar(32) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL COMMENT '图片',
  `sound` varchar(255) DEFAULT NULL COMMENT '声音',
  `label` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`question_id`),
  KEY `FK_Reference_8` (`area_id`),
  CONSTRAINT `FK_Reference_8` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='问题';

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES ('1', '1', '问题1', 'B', '2ff2ba10-cbc1-11e3-bf98-d9b713b1dfc1.jpg', 'null', '1', '2014-04-12 22:48:18', '2014-05-14 17:26:31', '0');
INSERT INTO `questions` VALUES ('2', '2', '问题2', 'C', '54422a50-cc4c-11e3-b923-7d3596959a65.jpg', '55aa9e80-cc4d-11e3-b923-7d3596959a65.mp3', '2', '2014-04-12 22:48:18', '2014-05-14 19:21:18', '0');
INSERT INTO `questions` VALUES ('3', null, '下面选项中，与播放音乐一致的是：', 'B', '484cd050-d90e-11e3-b21b-b9d95c4f7cd3.jpg', '5a74a410-d90e-11e3-b21b-b9d95c4f7cd3.mp3', '1', '2014-05-11 21:47:50', '2014-05-16 15:05:38', '0');
INSERT INTO `questions` VALUES ('4', null, '测试', 'A', '', '', 'undefined', '2014-05-16 15:30:52', '2014-05-16 17:02:54', '0');

-- ----------------------------
-- Table structure for `question_tag_relation`
-- ----------------------------
DROP TABLE IF EXISTS `question_tag_relation`;
CREATE TABLE `question_tag_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `question_id` int(11) DEFAULT NULL COMMENT 'ID',
  `tag_id` int(11) DEFAULT NULL COMMENT 'ID',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_10` (`tag_id`),
  KEY `FK_Reference_9` (`question_id`),
  CONSTRAINT `FK_Reference_10` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`),
  CONSTRAINT `FK_Reference_9` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='问题-标签关系表';

-- ----------------------------
-- Records of question_tag_relation
-- ----------------------------

-- ----------------------------
-- Table structure for `respondents`
-- ----------------------------
DROP TABLE IF EXISTS `respondents`;
CREATE TABLE `respondents` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `paper_id` int(11) DEFAULT NULL COMMENT 'ID',
  `question_id` int(11) DEFAULT NULL COMMENT 'ID',
  `answer` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`),
  KEY `FK_Reference_5` (`user_id`),
  KEY `FK_Reference_6` (`paper_id`),
  KEY `FK_Reference_7` (`question_id`),
  CONSTRAINT `FK_Reference_5` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `FK_Reference_6` FOREIGN KEY (`paper_id`) REFERENCES `papers` (`paper_id`),
  CONSTRAINT `FK_Reference_7` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='学生答卷';

-- ----------------------------
-- Records of respondents
-- ----------------------------
INSERT INTO `respondents` VALUES ('4', '1', '14', '1', 'B', '2014-05-15 11:34:52', '2014-05-15 11:34:52', '0');
INSERT INTO `respondents` VALUES ('5', '1', '14', '2', 'B', '2014-05-15 11:34:54', '2014-05-15 11:34:54', '0');
INSERT INTO `respondents` VALUES ('6', '1', '14', '3', 'B', '2014-05-15 11:34:56', '2014-05-15 11:34:56', '0');
INSERT INTO `respondents` VALUES ('7', '1', '1', '1', 'A', '2014-05-16 14:45:34', '2014-05-16 14:45:34', '0');
INSERT INTO `respondents` VALUES ('8', '1', '1', '3', 'B', '2014-05-16 17:16:44', '2014-05-16 17:16:44', '0');
INSERT INTO `respondents` VALUES ('9', '2', '1', '2', 'B', '2014-05-17 18:38:54', '2014-05-17 18:38:54', '0');
INSERT INTO `respondents` VALUES ('10', '2', '1', '3', 'B', '2014-05-17 18:38:58', '2014-05-17 18:39:12', '0');
INSERT INTO `respondents` VALUES ('11', '2', '1', '1', 'C', '2014-05-17 18:39:02', '2014-05-17 18:39:02', '0');

-- ----------------------------
-- Table structure for `tag`
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT '类别名称',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='题目标签';

-- ----------------------------
-- Records of tag
-- ----------------------------
INSERT INTO `tag` VALUES ('1', '标签', '2014-04-24 23:00:05', '2014-05-10 23:41:30', '0');
INSERT INTO `tag` VALUES ('2', '标签2', '2014-04-24 23:00:13', '2014-04-26 12:11:55', '1');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `nickname` varchar(32) DEFAULT NULL,
  `mobile` varchar(32) DEFAULT NULL COMMENT '手机',
  `taobao` varchar(255) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `sourse` varchar(32) DEFAULT NULL COMMENT '来源',
  `is_fee` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'frank', '13621370125', null, '4QrcOUm6Wau+VuBX8g+IPg==', null, '0', '2014-04-06 16:05:02', '2014-05-17 11:42:37', '0');
INSERT INTO `users` VALUES ('2', 'test2', '13001047235', null, '4QrcOUm6Wau+VuBX8g+IPg==', null, '1', '2014-04-07 22:54:25', '2014-05-17 11:42:28', '0');
INSERT INTO `users` VALUES ('3', 'frank2', '1300104723', 'frankly88', '4QrcOUm6Wau+VuBX8g+IPg==', null, '0', '2014-05-19 18:34:00', '2014-05-19 18:34:00', '0');
INSERT INTO `users` VALUES ('4', 'f', '13001047231', 'f', '4QrcOUm6Wau+VuBX8g+IPg==', null, '0', '2014-05-19 18:41:33', '2014-05-19 18:47:11', '0');
INSERT INTO `users` VALUES ('5', 'f', '13001047232', 'f', '4QrcOUm6Wau+VuBX8g+IPg==', null, '0', '2014-05-19 18:42:47', '2014-05-19 18:47:13', '0');
INSERT INTO `users` VALUES ('6', 'f', '13001047234', 'f', '4QrcOUm6Wau+VuBX8g+IPg==', null, '0', '2014-05-19 18:42:55', '2014-05-19 18:42:55', '0');
INSERT INTO `users` VALUES ('7', 'f', '13001047233', 'f', '4QrcOUm6Wau+VuBX8g+IPg==', null, '0', '2014-05-19 18:45:49', '2014-05-19 18:45:49', '0');
