/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : exam

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2014-07-10 16:06:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `musics`
-- ----------------------------
DROP TABLE IF EXISTS `musics`;
CREATE TABLE `musics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paper_id` int(11) DEFAULT NULL COMMENT '试卷id',
  `title` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL COMMENT '原名',
  `file` varchar(255) DEFAULT NULL COMMENT '新名',
  `author` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of musics
-- ----------------------------
INSERT INTO `musics` VALUES ('1', '16', null, '25.mp3', 'bd2d75a0-0762-11e4-a7b0-1fd781a0a49a.mp3', null, '2014-07-09 20:15:38', '2014-07-09 20:15:39', '0');
INSERT INTO `musics` VALUES ('2', '16', null, '21.mp3', 'cc0b7590-0762-11e4-a7b0-1fd781a0a49a.mp3', null, '2014-07-09 20:16:03', '2014-07-09 20:16:03', '0');
INSERT INTO `musics` VALUES ('3', '16', null, '22.mp3', 'd0e07d40-0762-11e4-a7b0-1fd781a0a49a.mp3', null, '2014-07-09 20:16:12', '2014-07-09 20:16:12', '0');
INSERT INTO `musics` VALUES ('4', '16', null, '23.mp3', 'd39c1ad0-0762-11e4-a7b0-1fd781a0a49a.mp3', null, '2014-07-09 20:16:16', '2014-07-09 20:16:16', '0');
INSERT INTO `musics` VALUES ('5', '16', null, '24.mp3', 'd62cb0c0-0762-11e4-a7b0-1fd781a0a49a.mp3', null, '2014-07-09 20:16:20', '2014-07-09 20:16:20', '0');
INSERT INTO `musics` VALUES ('6', '16', null, '25.mp3', 'd8b382b0-0762-11e4-a7b0-1fd781a0a49a.mp3', null, '2014-07-09 20:16:25', '2014-07-09 20:16:25', '0');
INSERT INTO `musics` VALUES ('7', '16', null, '26.mp3', 'dbc46c30-0762-11e4-a7b0-1fd781a0a49a.mp3', null, '2014-07-09 20:16:30', '2014-07-09 20:16:30', '0');
INSERT INTO `musics` VALUES ('8', '16', null, '27.mp3', 'de5bdff0-0762-11e4-a7b0-1fd781a0a49a.mp3', null, '2014-07-09 20:16:34', '2014-07-09 20:16:34', '0');
INSERT INTO `musics` VALUES ('9', '16', null, '28.mp3', 'e0fa5890-0762-11e4-a7b0-1fd781a0a49a.mp3', null, '2014-07-09 20:16:39', '2014-07-09 20:16:39', '0');
INSERT INTO `musics` VALUES ('10', '16', null, '29.mp3', 'e3e395d0-0762-11e4-a7b0-1fd781a0a49a.mp3', null, '2014-07-09 20:16:43', '2014-07-09 20:16:43', '0');
INSERT INTO `musics` VALUES ('11', '16', null, '30.mp3', 'e6ff54c0-0762-11e4-a7b0-1fd781a0a49a.mp3', null, '2014-07-09 20:16:49', '2014-07-09 20:16:49', '0');
INSERT INTO `musics` VALUES ('12', '16', null, '31.mp3', 'e9a03e60-0762-11e4-a7b0-1fd781a0a49a.mp3', null, '2014-07-09 20:16:53', '2014-07-09 20:16:53', '0');
INSERT INTO `musics` VALUES ('18', '16', null, '32.mp3', 'fcadf1f0-07e9-11e4-ac44-7b95723e7a1e.mp3', null, '2014-07-10 12:23:47', '2014-07-10 12:23:47', '0');
INSERT INTO `musics` VALUES ('19', '16', null, '32.mp3', '00b374a0-07ea-11e4-ac44-7b95723e7a1e.mp3', null, '2014-07-10 12:23:54', '2014-07-10 12:23:54', '0');
INSERT INTO `musics` VALUES ('20', '16', null, '33.mp3', '05520300-07ea-11e4-ac44-7b95723e7a1e.mp3', null, '2014-07-10 12:24:02', '2014-07-10 12:24:02', '0');
INSERT INTO `musics` VALUES ('21', '16', null, '34.mp3', '09473200-07ea-11e4-ac44-7b95723e7a1e.mp3', null, '2014-07-10 12:24:08', '2014-07-10 12:24:08', '0');
INSERT INTO `musics` VALUES ('22', '16', null, '35.mp3', '0cb381f0-07ea-11e4-ac44-7b95723e7a1e.mp3', null, '2014-07-10 12:24:14', '2014-07-10 12:24:14', '0');
INSERT INTO `musics` VALUES ('23', '16', null, '36.mp3', '10c62400-07ea-11e4-ac44-7b95723e7a1e.mp3', null, '2014-07-10 12:24:21', '2014-07-10 12:24:21', '0');
INSERT INTO `musics` VALUES ('24', '16', null, '37.mp3', '149b9600-07ea-11e4-ac44-7b95723e7a1e.mp3', null, '2014-07-10 12:24:27', '2014-07-10 12:24:27', '0');
