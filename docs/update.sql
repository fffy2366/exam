ALTER TABLE questions ADD COLUMN correct VARCHAR(32) AFTER title ;

添加表：graded classification

ALTER TABLE users ADD COLUMN taobao varchar(255) AFTER mobile ;


#创建账号
grant all privileges on exam.* to exam@localhost identified by 'exam2014';
#创建数据库
CREATE DATABASE exam DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
#导入数据库
mysql -uexam -p exam </opt/www/exam_20140520.sql  --default-character-set=utf8



#2014-06-09
ALTER TABLE questions ADD COLUMN settings text AFTER sound ;

#--20140628---
ALTER TABLE papers ADD COLUMN is_free int(11) DEFAULT 0 AFTER title ;


#20140703
#ALTER TABLE questions ADD COLUMN num int(11) DEFAULT 0 AFTER title ;

#ALTER TABLE questions ADD COLUMN score int(11) DEFAULT 0 AFTER sound ;


#ALTER TABLE questions ADD COLUMN titlesound VARCHAR(255)  AFTER sound;

#UPDATE questions SET created_at = '2014-07-03 03:44:01' WHERE question_id >51 ;

#20140705
#ALTER TABLE users ADD COLUMN settings text AFTER sourse ;

#SELECT * FROM users WHERE mobile = '13001047235' ;

#ALTER TABLE users ADD COLUMN power BIGINT(64) AFTER sourse ;

#20140708
#ALTER TABLE papers ADD COLUMN is_music TINYINT(1) DEFAULT 0 COMMENT '是否是音乐列表' AFTER is_free ;

#ALTER TABLE users ADD COLUMN class_power BIGINT(64) COMMENT '分类权限' AFTER power ;

#table musics
#20140708
#ALTER TABLE questions ADD COLUMN category_id int(11) DEFAULT 0 AFTER area_id ;
#table category

#20140810
#ALTER TABLE users ADD COLUMN alipay VARCHAR(32) COMMENT '支付宝账号' AFTER taobao ;

#20141229
ALTER TABLE admin ADD COLUMN mobile VARCHAR(32) AFTER PASSWORD ;


#20150305
ALTER TABLE users ADD COLUMN exam_time VARCHAR(32) AFTER alipay ;
ALTER TABLE users ADD COLUMN address VARCHAR(255) AFTER exam_time ;


#20150314
ALTER TABLE `code` ADD COLUMN ip VARCHAR(32) AFTER code ;

