/**
 * 验证码ccap
 * author:Frank
 * date:2014-05-03
 * e-mail:fengxuting@gmail.com
 */
var ccap = require('ccap') ;
module.exports = function(req, res){
	var captcha = ccap({

	    width:96,//set width,default is 256

	    height:35,//set height,default is 60

	    offset:20,//set text spacing,default is 40

	    quality:90,//set pic quality,default is 50

	    fontsize:32,//set font size,default is 57

	    generate:function(){//Custom the function to generate captcha text

	        //generate captcha text here
	        //var text = "1234" ;
			var s = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
			var text = '';
			for(var i=0;i<4;i++){
			    text+= s.substr(parseInt(Math.random()*36),1);
			}

	         return text;//return the captcha text

	    }

	});		
	var ary = captcha.get();
	var txt = ary[0];
	var buf = ary[1];
	console.log(txt);
	res.end(buf) ;
}