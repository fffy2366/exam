
var logger = require('../Logger.js');
var moment = require('moment');
var Pager = require('../Pager') ;
var StringUtils = require('../StringUtils') ;
var uuid = require('node-uuid') ;
var async = require('async') ;

var Question = require('../models/question.js') ;
var Answer = require('../models/answer.js') ;
var PaperQuestion = require('../models/paper.question.js') ;
var Tag = require('../models/tag.js') ;
var Paper = require('../models/paper.js') ;

var Classification = require('../models/classification.js') ;
var Graded = require('../models/graded.js') ;


module.exports = function(app) {
	//后台试卷管理
	app.get('/admin/paper',function(req, res){
		var pageSize = 10 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		Paper.findAllByPage(currPage, pageSize, keywords,'','', function(err,totalCount, papers){
			if(err){
				logger.info("find all error") ;
			}
			logger.info(" totalCount---->:"+totalCount) ;
			//var totalCount = users.length ;
			Graded.findAll(function(err, graded){
				if(err){
					logger.info("find graded error") ;
					res.send("error") ;
				}
				Classification.findAll(function(err,classification){
					if(err){
						logger.info("find graded error") ;
						res.send("error") ;
					}
					res.render('admin/papers',{
						title:"试卷管理",
						papers:papers,
						keywords:keywords,
						'graded':graded,
						'classification':classification,
						moment:moment,
						currPage:currPage,
						pager:Pager(req, totalCount, pageSize, currPage)
					}); 			
				});
			});
		}) ;
	}) ;
	//后台试卷添加页
	app.get('/admin/paper/add',function(req,res){
		Graded.findAll(function(err, graded){
			if(err){
				logger.info("find graded error") ;
				res.send("error") ;
			}
			Classification.findAll(function(err,classification){
				if(err){
					logger.info("find graded error") ;
					res.send("error") ;
				}
				res.render('admin/papers-edit',{
					'graded':graded,
					'classification':classification
				}) ;				
			});
		});
		
	}) ;
	//后台试卷编辑页
	app.get('/admin/paper/edit/:id',function(req, res){
		var id = req.params.id ;
		Paper.findById(id, function(err, paper){
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			if(paper){
				logger.info("paper:"+paper) ;
				//console.log(paper[0]) ;
				Graded.findAll(function(err, graded){
					if(err){
						logger.info("find graded error") ;
						res.send("error") ;
					}
					Classification.findAll(function(err,classification){
						if(err){
							logger.info("find graded error") ;
							res.send("error") ;
						}
						res.render('admin/papers-edit',{
							title:"会员编辑",
							id:id,			
							paper:paper[0],
							'graded':graded,
							'classification':classification
						}); 							
					});
				});
			}
		}) ;
	}) ;
	//后台试卷编辑
	app.post('/admin/paper/update', function(req, res){
		var title = req.body.title ;
		var is_free = req.body.is_free ;
		var is_music = req.body.is_music ;
		var graded_id = req.body.graded_id ;
		var classification_id = req.body.classification_id ;
		var description = req.body.description ;

		var id = req.body.id ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		if(id){
			Paper.update(id,title,is_free,is_music,graded_id,classification_id, description, function(err, result){
				if(err){
					res.send({status:"n",info:"试卷更新失败"}) ;	
				}else{
					res.send({status:"y",info:"试卷更新成功"}) ;	
				}
			}) ;
		}else{
			Paper.save(title, is_free, is_music, graded_id,classification_id,description, created_at ,function(err, result){
				if(err){
					res.send({status:"n",info:"试卷添加失败"}) ;	
				}else{
					res.send({status:"y",info:"试卷添加成功"}) ;
				}
			}) ;			
		}
	}) ;
	//试卷删除
	app.post('/admin/paper/delete', function(req, res){
		var pid = req.body.id ;
		Paper.deleteById(pid, function(err, result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除试卷失败"}) ;
			}
			res.send({status:"y",info:"删除试卷成功"}) ;
		}) ;
	}) ;
	//试卷题目
	app.get('/admin/paper-questions',function(req, res){
		var pid = req.query.pid ;
		var pageSize = 10 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		Paper.findById(pid, function(err, paper){
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			if(paper){
				logger.info("paper:"+paper) ;
				//console.log(paper[0]) ;
				PaperQuestion.findAllByPage(pid,currPage, pageSize, keywords, function(err,totalCount, questions){
					if(err){
						logger.info("find all error") ;
					}
					logger.info(" totalCount---->:"+totalCount) ;
					//var totalCount = users.length ;
					Tag.findAll(function(err, tags){
						if (err) {
							logger.info("tag error") ;
						}
						res.render('admin/paper-questions',{
							title:"试卷管理",
							pid:pid,
							paper:paper[0],
							questions:questions,
							keywords:keywords,
							moment:moment,
							currPage:currPage,
							pageSize:pageSize,
							tags:tags,
							pager:Pager(req, totalCount, pageSize, currPage)
						}); 							
					}) ;

				}) ;
			}
		}) ;
	}) ;
	//添加试题到试卷
	app.post('/admin/paper-question/add',function(req, res){
		var p_id = req.body.p_id ;
		var q_ids = req.body.q_ids ;
		//console.log(p_id) ;
		//console.log(q_ids) ;

		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		//for(var key in q_ids){
		//}
		async.each(q_ids, function(q_id,callback){
			logger.info("for:"+q_id) ;
			//是否存在，存在continue，不存在判断题目总数，如果少于40题则添加
			PaperQuestion.findById(p_id,q_id, function(err,result){
				if(err){
					logger.info("findById error") ;
					res.send({status:"n",info:"试题添加到试卷失败"}) ;
					return ;
				}
				logger.info('length:'+result.length) ;
				if(result.length>0){
					//continue ;
					//console.log(result) ;
				}else{
					//添加
					logger.info("add:"+q_id) ;
					
					PaperQuestion.save(p_id,q_id,created_at,function(err,result){
						if(err){
							logger.info("save error") ;
							res.send({status:"n",info:"试题添加到试卷失败"}) ;
							return ;
						}
		 				
					}) ;
				}
			}) ;
			res.send({status:"y",info:"试题成功添加到试卷"}) ;
		});
	}) ;
	//从试卷删除题目
	app.post('/admin/paper-questions/delete',function(req, res){
		var pid = req.body.pid ;
		var qid = req.body.qid ;
		PaperQuestion.deleteById(pid, qid, function(err, result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除题目失败"}) ;
			}
			res.send({status:"y",info:"删除题目成功"}) ;
		}) ;
	})
}