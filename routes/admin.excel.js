var path = require('path');
var logger = require('../Logger.js');
var moment = require('moment');
var Pager = require('../Pager') ;
var StringUtils = require('../StringUtils') ;
var uuid = require('node-uuid') ;
var async = require('async') ;

var Question = require('../models/question.js') ;
var Answer = require('../models/answer.js') ;
var PaperQuestion = require('../models/paper.question.js') ;
var Tag = require('../models/tag.js') ;
var Paper = require('../models/paper.js') ;

var Classification = require('../models/classification.js') ;
var Graded = require('../models/graded.js') ;


module.exports = function(app) {
	//后台试题导入管理
	app.get('/admin/excel',function(req, res){
		var xlsx = require('node-xlsx');

		var obj = xlsx.parse(path.dirname(__dirname) + '/public/uploads/myFile.xlsx'); // parses a file

		//var obj = xlsx.parse(fs.readFileSync(__dirname + '/myFile.xlsx')); // parses a buffer
		res.send(obj) ;
		/*
		var excelParser = require('excel-parser');
		excelParser.worksheets({
		  inFile: path.dirname(__dirname) + '/public/uploads/myFile.xlsx'
		}, function(err, worksheets){
		  if(err) console.error(err);
		  console.log(worksheets);
		});		*/
	}) ;
}