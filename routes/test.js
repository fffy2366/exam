/**
 * 前台测试
 */
var http = require('http') ;
var logger = require('../Logger') ;
var moment = require('moment');
var Pager = require('../Pager') ;
var StringUtils = require('../StringUtils') ;
var uuid = require('node-uuid') ;
var async = require('async') ;

var Paper = require('../models/paper.js') ;
var Question = require('../models/question.js') ;
var Answer = require('../models/answer.js') ;
var PaperQuestion = require('../models/paper.question.js') ;
var Classification = require('../models/classification.js') ;
var Graded = require('../models/graded.js') ;
var Respondents = require('../models/respondents.js') ;
var User = require('../models/user.js') ;
var Music = require('../models/music.js') ;
var Options = require('../models/options') ;

module.exports = function(app) {
	//分级列表
	app.get('/list/fenji',function(req, res){
		list(req,res,'graded',1) ;
	});
	app.get('/list/fenji/:id',function(req, res){
		var id = req.params.id ;
		list(req,res,'graded',id) ;
	});
	//分类列表
	app.get('/list/fenlei',function(req, res){
		var id = req.params.id ;
		list(req,res,'classification',1) ;
	});
	app.get('/list/fenlei/:id',function(req, res){
		var id = req.params.id ;
		list(req,res,'classification',id) ;
	});
	//历史查询
	app.get('/list/history',function(req, res){
		var user_id = req.session.user.user_id ;
		Respondents.findPaper(user_id,function(err,papers){
			if(err){
				logger.error("find paper error:"+err) ;
			}
			Options.findByName('notice', function(err, notice){
				if(err){
					console.log("err") ;
				}
				res.render('hsearch', {
					papers:papers,
					notice:notice,
					title: '分级'
				});
			});
		}) ;
	});
	//试卷
	app.get('/paper/:id',function(req, res){
		var id = req.params.id ;
		PaperQuestion.findById(id,'',function(err, paperQuestions){
			if(err){
				logger.info("find PaperQuestion error") ;
			}	
			logger.info("length:"+paperQuestions.length) ;
			res.render('test', {
				title: '分级',
				id:id,
				paperCount:paperQuestions.length
			});					
		});
	});
	//音乐列表
	app.get('/paper/music/:id',function(req, res){
		var pid = req.params.id ;
		Music.findAllByPid(pid,function(err,musics){
			if (err) {
				logger.info("find error") ;
			}
			res.render('music',{
				pid:pid,
				musics:musics
			});
		});

	});
	//题目 前台ajax 获取第N道题目
	app.post('/paper-question',function(req, res){
		var paper_id = req.body.paper_id ;
		var id = req.body.id ;//第几道题
		var currPage = 1 ;
		var pageSize = 100 ;//每套试卷的题目数
		var keywords = "" ;
		var user_id = req.session.user.user_id ;
		logger.info("id:"+id) ;
		PaperQuestion.findAllByPage(paper_id,currPage, pageSize, keywords, function(err,totalCount, questions){
			if(err){
				logger.info("find all error") ;
			}
			var question = questions[(id-1)] ;
			//console.log(questions) ;
			//console.log(question) ;
			if(!!question){
				//题目选项
				Answer.findByQuestionId(question.question_id, function(err, answers){
					//console.log("answers:"+answers) ;
					if(err){
						res.send("error") ;
					}
					question.answers = answers ;
					//是否已经答过，如果答过回填
					Respondents.findById(user_id, paper_id, question.question_id, function(err, respondents){
						if(err){
							res.send("find by id respondents error") ;
						}
						question.respondents = respondents[0] ;
						res.send(question) ;
					}) ;					
				}) ;				
			}else{
				res.send("") ;
			}

		});
	}) ;
	//答题记录
	app.post('/respondents',function(req, res){
		var paper_id = req.body.paper_id ;
		var question_id = req.body.question_id ;
		var option = req.body.option ;
		var user_id = req.session.user.user_id ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		//logger.info("user_id:"+user_id) ;
		//logger.info(req.body) ;
		//判断是否
		Respondents.update(user_id, paper_id, question_id, option,created_at, function(err,respondents){
			if(err){
				logger.info("update error") ;
			}
			res.send("success") ;
		});
		
	}) ;
	//答题结果
	app.get('/testresult/:id',function(req, res){
		var user_id = req.session.user.user_id ;
		var id = req.params.id ;
		var currPage = 1 ;
		var pageSize = 100 ;//每套试卷的题目数
		var keywords = "" ;
		PaperQuestion.findAllByPage(id,currPage, pageSize, keywords, function(err,totalCount, questions){
			if(err){
				logger.info("find PaperQuestion error") ;
			}	
			logger.info("length:"+questions.length) ;
			var correct = [];
			var wrong = [] ;
			var score = 0 ;
			var i = 0 ;
			async.eachSeries(questions, function(q,callback){
				logger.info("q:"+q) ;
				//console.log(q) ;
				i++ ;	
				var a = function(i){
					Respondents.findById(user_id, id, q.question_id, function(err, respondents){
						if(err){
							res.send("find by id respondents error") ;
						}
						//logger.info("respondents[0].answer:"+respondents[0].answer) ;
						logger.info("q.correct:"+q.correct) ;
						if(!!respondents[0]&&respondents[0].answer==q.correct){
							logger.info("correct---->") ;
							correct.push(i) ;
							score = score+parseInt(q.score) ;
						}else{
							logger.info("wrong---->") ;
							wrong.push(i) ;
						}
						callback(err, correct, wrong) ;
					}) ;
				}
				a(i) ;											
				logger.info("i---->:"+i) ;
			}, function(err){
				if(err){
					logger.info(err) ;
				}else{				
					logger.info("render=====correct:"+correct+"wrong:"+wrong) ;				
					res.render('testresult', {
						title: '答题结果',
						id:id,
						correct:correct,
						wrong:wrong,
						score:score,
						paperCount:questions.length
					});				
				}
			});
		});
	});
	//删除考试记录
	app.post('/resettest',function(req, res){
		var user_id = req.session.user.user_id ;
		var paper_id = req.body.paper_id ;
		Respondents.delete(user_id,paper_id,function(err,results){
			if(err){
				logger.error('respondents delete err:'+err) ;
			}
			res.send({'status':'y','info':'success'}) ;
		}) ;
	}) ;
	//分级显示答案
	app.post('/showanswer',function(req, res){
		var paper_id = req.body.paper_id ;
		var question_id = req.body.question_id ;
		var option = req.body.option ;
		Paper.findById(paper_id,function(err,results){
			if(results[0].classification_id>0){
				Question.findById(question_id,function(err, results2){
					if(option==results2[0].correct){
						res.send({'ret':'1','code':'right'}) ;
					}else{
						res.send({'ret':'2','code':'not class','right':results2[0].correct}) ;
					}
				}) ;
			}else{
				res.send({'ret':'3','code':'not class'}) ;
			}
		}) ;
	}) ;

}


/**
 * 分级 分类
 */
function list(req, res, type, id){
	logger.info("id:"+id) ;
	//var pageSize = 18 ;
	var pageSize = 5 ;
	logger.info("currPage---->:"+req.query.page) ;
	var currPage = !req.query['page']?"1":req.query['page'] ;
	var keywords = !req.query['keywords']?"":req.query['keywords'] ;
	Graded.findAll(function(err, graded){
		if(err){
			logger.info("find graded error") ;
			res.send("error") ;
		}
		var _graded = [] ;
		var power = req.session.user.power ;
		if(!!power){
			graded.forEach(function(grd){
				if(Math.pow(2,grd.graded_id)&power){
					_graded.push(grd) ;
				}
			}) ;
		}
		if(type=="graded"&&id==1){
			id = !!_graded[0]&&!!_graded[0].graded_id?_graded[0].graded_id:1000 ;//1000是为了Paper.findAllByPage查询无结果
		}
		Classification.findAll(function(err,classification){
			if(err){
				logger.info("find graded error") ;
				res.send("error") ;
			}
			logger.info("classification:"+classification) ;
			var _classification = [] ;
			var class_power = req.session.user.class_power ;
			if(!!class_power){
				classification.forEach(function(cla){
					if(Math.pow(2,cla.classification_id)&class_power){
						_classification.push(cla) ;
					}
				}) ;
			}
			if(type=="classification"&&id==1){
				id = !!_classification[0]&&!!_classification[0].classification_id?_classification[0].classification_id:1000 ;//1000是为了Paper.findAllByPage查询无结果
			}
			logger.info("id:"+id) ;
			Paper.findAllByPage(currPage, pageSize, keywords,type,id ,function(err,totalCount, papers){
				if(err){
					logger.info("find all error") ;
				}
				logger.info(" totalCount---->:"+totalCount) ;
				//var totalCount = users.length ;
				Options.findByName('notice', function(err, notice){
					if(err){
						console.log("err") ;
					}
					res.render(type=='graded'?'fenji':'fenlei',{
						title:"试卷列表",
						papers:papers,
						keywords:keywords,
						id:id,
						'graded':_graded,
						'classification':_classification,
						moment:moment,
						notice:notice,
						currPage:currPage,
						pager:Pager(req, totalCount, pageSize, currPage)
					}) ;
				}) ;			

			}); 								
		});
	});
}