var path = require('path');
var fs = require('fs') ; 
var logger = require('../Logger.js');
var moment = require('moment');
var Pager = require('../Pager') ;
var StringUtils = require('../StringUtils') ;
var uuid = require('node-uuid') ;
var async = require('async') ;
var Graded = require('../models/graded.js') ;
var Music = require('../models/music.js') ;

module.exports = function(app) {
	// 后台music管理
	app.get('/admin/music',function(req, res){
		var pid = req.query.pid ;
		Music.findAllByPid(pid,function(err,musics){
			if (err) {
				logger.info("find error") ;
			}
			res.render('admin/music',{
				title:"music管理",
				pid:pid,
				musics:musics
			});
		});
	}) ;
	//上传
	app.post('/admin/music/upload', function(req, res){
		res.setHeader('Content-Type','text/html');
		res.setHeader('charset','utf-8');
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		var limitMaxSize = req.body.limitMaxSize ;
		var type = req.body.type ;
		var pid = req.body.pid ;
		logger.info("limitMaxSize:"+limitMaxSize) ;
		var upfile = req.files.upfile;
	    var files = [];
	    if (upfile instanceof  Array) {
	        files = upfile;
	    } else {
	        files.push(upfile);
	    }
	    var fileSize = files.length ;
	    for (var i = 0; i < fileSize; i++) {
	        var file = files[i];
	        logger.info("file"+file) ;
	        //console.log(file) ;
	        //console.log(file.headers) ;
	        var src_path = file.path;
	        var name = file.name;
	        var ext = name.split(".") ;
	        if(ext.length>1){
	        	ext = ext[1] ;
	        }
	        logger.info("ext:"+ext) ;
	        //var target_path = path.join(path.dirname(__dirname), "/public/uploads/" + name) ;
	        //StringUtils.mkdir('./public/uploads/'+type, function(err) {logger.info(err);});
	        StringUtils.mkdirSync('./public/uploads/'+type);
	        //var target_path = "./public/uploads/questions/" + name ;
	        var newFile = uuid.v1()+"."+ext ;
	        var target_path = "./public/uploads/"+type+"/" + newFile ;
	        logger.info("target_path:"+target_path) ;
	        logger.info("src_path:"+src_path) ;
	        
	        fs.rename(src_path, target_path, function (err) {
	            if (err) {
	            	throw err;
	            	res.send({status:"n",info:"上传失败"}) ;
	            }
	            Music.save(pid,name,newFile,created_at,function(err){
	            	if (err) {
	            		res.send({status:"n",info:"上传失败"}) ;
	            	}
		            logger.info("i:"+i) ;//为啥i=1
					res.send({status:"y",info:"上传成功","path":target_path,"filename":newFile}) ;	
	            }) ;
	        });
	    }
	}) ;	
	app.post('/admin/music/delete', function(req, res){
		var file = req.body.file ;
		Music.deleteByFile(file, function(err, result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除音乐失败"}) ;
			}else{
				StringUtils.removeFile("./public/uploads/musics/"+file,function(info){
					logger.info("info："+info) ;
					if(info=="success"){
						res.send({status:"y",info:"删除音乐成功"}) ;		
					}else if(info=="nofile"){
						res.send({status:"y",info:"音乐文件不存在成功"}) ;	
					}else{
						res.send({status:"n",info:"删除音乐失败"}) ;
						//res.send({status:"y",info:"删除音乐成功"}) ;//正确应该返回失败，但因为数据库已经删除故返回成功。
					}
				}) ;
						
			}
		}) ;		
	}) ;
}