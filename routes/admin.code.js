var logger = require('../Logger.js');
var moment = require('moment');
var Pager = require('../Pager') ;
var StringUtils = require('../StringUtils') ;
var uuid = require('node-uuid') ;
var async = require('async') ;
var Code = require('../models/code.js') ;

module.exports = function(app) {
	//后台题库目录管理
	app.get('/admin/code',function(req, res){
		var pid = req.query['pid'] ;
		var pageSize = 10 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		Code.findAllByPage(currPage, pageSize, keywords, function(err,totalCount, code){
			if(err){
				logger.info("find all error") ;
			}
			logger.info(" totalCount---->:"+totalCount) ;
			//var totalCount = users.length ;
			res.render('admin/code',{
				title:"题库目录管理",
				code:code,
				pid:pid,
				keywords:keywords,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			}); 			
		}) ;
	}) ;
	//后台题库目录添加页
	app.get('/admin/code/add',function(req,res){
		res.render('admin/code-edit') ;
	}) ;
	//后台题库目录编辑页
	app.get('/admin/code/edit/:id',function(req, res){
		var id = req.params.id ;
		Code.findById(id, function(err, code){
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			if(code){
				logger.info("code:"+code) ;
				//console.log(code[0]) ;
				res.render('admin/code-edit',{
					title:"会员编辑",
					id:id,			
					code:code[0]
				}); 							
			}
		}) ;
	}) ;
	//后台题库目录编辑
	app.post('/admin/code/update', function(req, res){
		var code = req.body.code ;
		var id = req.body.id ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		if(id){
			Code.update(id,code, function(err, result){
				if(err){
					res.send({status:"n",info:"更新失败"}) ;	
				}else{
					res.send({status:"y",info:"更新成功"}) ;	
				}
			}) ;
		}else{
			Code.save(code, created_at ,function(err, result){
				if(err){
					res.send({status:"n",info:"题库目录添加失败"}) ;	
				}else{
					res.send({status:"y",info:"题库目录添加成功"}) ;	
				}
			}) ;			
		}
	}) ;
	//后台题库目录删除
	app.post('/admin/code/delete', function(req, res){
		var qid = req.body.id ;
		Code.deleteById(qid, function(err, result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除题库目录失败"}) ;
			}else{
				res.send({status:"y",info:"删除题库目录成功"}) ;				
			}
		}) ;
	}) ;
}