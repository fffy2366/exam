var logger = require('../Logger.js');
var moment = require('moment');
var Pager = require('../Pager') ;
var StringUtils = require('../StringUtils') ;
var uuid = require('node-uuid') ;
var async = require('async') ;
var Loginlog = require('../models/loginlog.js') ;

module.exports = function(app) {
	//后台登录日志管理
	app.get('/admin/loginlog',function(req, res){
		var pid = req.query['pid'] ;
		var pageSize = 10 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		Loginlog.findAllByPage(currPage, pageSize, keywords, function(err,totalCount, loginlog){
			if(err){
				logger.info("find all error") ;
			}
			logger.info(" totalCount---->:"+totalCount) ;
			//var totalCount = users.length ;
			res.render('admin/loginlog',{
				title:"登录日志管理",
				loginlog:loginlog,
				pid:pid,
				keywords:keywords,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			}); 			
		}) ;
	}) ;
	//后台登录日志添加页
	app.get('/admin/loginlog/add',function(req,res){
		res.render('admin/loginlog-edit') ;
	}) ;
	//后台登录日志编辑页
	app.get('/admin/loginlog/edit/:id',function(req, res){
		var id = req.params.id ;
		Loginlog.findById(id, function(err, loginlog){
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			if(loginlog){
				logger.info("loginlog:"+loginlog) ;
				//console.log(loginlog[0]) ;
				res.render('admin/loginlog-edit',{
					title:"会员编辑",
					id:id,			
					loginlog:loginlog[0]
				}); 							
			}
		}) ;
	}) ;
	//后台登录日志编辑
	app.post('/admin/loginlog/update', function(req, res){
		var loginlog = req.body.loginlog ;
		var id = req.body.id ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		if(id){
			Loginlog.update(id,loginlog, function(err, result){
				if(err){
					res.send({status:"n",info:"更新失败"}) ;	
				}else{
					res.send({status:"y",info:"更新成功"}) ;	
				}
			}) ;
		}else{
			Loginlog.save(loginlog, created_at ,function(err, result){
				if(err){
					res.send({status:"n",info:"登录日志添加失败"}) ;	
				}else{
					res.send({status:"y",info:"登录日志添加成功"}) ;	
				}
			}) ;			
		}
	}) ;
	//后台登录日志删除
	app.post('/admin/loginlog/delete', function(req, res){
		var qid = req.body.id ;
		Loginlog.deleteById(qid, function(err, result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除登录日志失败"}) ;
			}else{
				res.send({status:"y",info:"删除登录日志成功"}) ;				
			}
		}) ;
	}) ;
}