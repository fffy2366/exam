var logger = require('../Logger.js');
var moment = require('moment');
var Pager = require('../Pager') ;
var StringUtils = require('../StringUtils') ;
var uuid = require('node-uuid') ;
var async = require('async') ;
var Category = require('../models/category.js') ;

module.exports = function(app) {
	//后台题库目录管理
	app.get('/admin/category',function(req, res){
		var pid = req.query['pid'] ;
		var pageSize = 10 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		Category.findAllByPage(currPage, pageSize, keywords, function(err,totalCount, category){
			if(err){
				logger.info("find all error") ;
			}
			logger.info(" totalCount---->:"+totalCount) ;
			//var totalCount = users.length ;
			res.render('admin/category',{
				title:"题库目录管理",
				category:category,
				pid:pid,
				keywords:keywords,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			}); 			
		}) ;
	}) ;
	//后台题库目录添加页
	app.get('/admin/category/add',function(req,res){
		res.render('admin/category-edit') ;
	}) ;
	//后台题库目录编辑页
	app.get('/admin/category/edit/:id',function(req, res){
		var id = req.params.id ;
		Category.findById(id, function(err, category){
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			if(category){
				logger.info("category:"+category) ;
				//console.log(category[0]) ;
				res.render('admin/category-edit',{
					title:"会员编辑",
					id:id,			
					category:category[0]
				}); 							
			}
		}) ;
	}) ;
	//后台题库目录编辑
	app.post('/admin/category/update', function(req, res){
		var category = req.body.category ;
		var id = req.body.id ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		if(id){
			Category.update(id,category, function(err, result){
				if(err){
					res.send({status:"n",info:"更新失败"}) ;	
				}else{
					res.send({status:"y",info:"更新成功"}) ;	
				}
			}) ;
		}else{
			Category.save(category, created_at ,function(err, result){
				if(err){
					res.send({status:"n",info:"题库目录添加失败"}) ;	
				}else{
					res.send({status:"y",info:"题库目录添加成功"}) ;	
				}
			}) ;			
		}
	}) ;
	//后台题库目录删除
	app.post('/admin/category/delete', function(req, res){
		var qid = req.body.id ;
		Category.deleteById(qid, function(err, result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除题库目录失败"}) ;
			}else{
				res.send({status:"y",info:"删除题库目录成功"}) ;				
			}
		}) ;
	}) ;
}