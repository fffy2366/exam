var logger = require('../Logger.js');
var moment = require('moment');
var Pager = require('../Pager') ;
var StringUtils = require('../StringUtils') ;
var uuid = require('node-uuid') ;
var async = require('async') ;

var Classification = require('../models/classification.js') ;

module.exports = function(app) {
	//后台分类管理
	app.get('/admin/classification',function(req, res){
		var pageSize = 10 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		Classification.findAllByPage(currPage, pageSize, keywords, function(err,totalCount, classification){
			if(err){
				logger.info("find all error") ;
			}
			logger.info(" totalCount---->:"+totalCount) ;
			//var totalCount = users.length ;
			res.render('admin/classification',{
				title:"分类管理",
				classification:classification,
				keywords:keywords,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			}); 			
		}) ;
	}) ;
	//后台分类添加页
	app.get('/admin/classification/add',function(req,res){
		res.render('admin/classification-edit') ;
	}) ;
	//后台分类编辑页
	app.get('/admin/classification/edit/:id',function(req, res){
		var id = req.params.id ;
		Classification.findById(id, function(err, tag){
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			if(tag){
				logger.info("tag:"+tag) ;
				//console.log(tag[0]) ;
				res.render('admin/classification-edit',{
					title:"会员编辑",
					id:id,			
					tag:tag[0]
				}); 							
			}
		}) ;
	}) ;
	//后台分类编辑
	app.post('/admin/classification/update', function(req, res){
		var tagname = req.body.tagname ;
		var id = req.body.id ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		if(id){
			Classification.update(id,tagname, function(err, result){
				if(err){
					res.send({status:"n",info:"更新失败"}) ;	
				}else{
					res.send({status:"y",info:"更新成功"}) ;	
				}
			}) ;
		}else{
			Classification.save(tagname, created_at ,function(err, result){
				if(err){
					res.send({status:"n",info:"分类添加失败"}) ;	
				}else{
					res.send({status:"y",info:"分类添加成功"}) ;	
				}
			}) ;			
		}
	}) ;
	//后台分类删除
	app.post('/admin/classification/delete', function(req, res){
		var qid = req.body.id ;
		Classification.deleteById(qid, function(err, result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除分类失败"}) ;
			}else{
				res.send({status:"y",info:"删除分类成功"}) ;				
			}
		}) ;
	}) ;
}