var logger = require('../Logger.js');
var moment = require('moment');
var Pager = require('../Pager') ;
var StringUtils = require('../StringUtils') ;
var uuid = require('node-uuid') ;
var async = require('async') ;
var News = require('../models/news.js') ;
/**
 * cate_id: 视频1 图片2 答题卡3
 * modified on 2014-12-22
 * cate_id: 视频1 初级图片2 答题卡3 中级图片4
 */
module.exports = function(app) {
	//============视频 start===========
	//后台视频管理
	app.get('/admin/video',function(req, res){
		var pid = req.query['pid'] ;
		var pageSize = 10 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		News.findAllByPage(1,currPage, pageSize, keywords, function(err,totalCount, video){
			if(err){
				logger.info("find all error") ;
			}
			logger.info(" totalCount---->:"+totalCount) ;
			//var totalCount = users.length ;
			res.render('admin/video',{
				title:"视频管理管理",
				video:video,
				pid:pid,
				keywords:keywords,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			}); 			
		}) ;
	}) ;
	//后台视频管理添加页
	app.get('/admin/video/add',function(req,res){
		res.render('admin/video-edit') ;
	}) ;
	//后台视频管理编辑页
	app.get('/admin/video/edit/:id',function(req, res){
		var id = req.params.id ;
		News.findById(id, function(err, video){
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			if(video){
				logger.info("video:"+video) ;
				//console.log(video[0]) ;
				res.render('admin/video-edit',{
					title:"会员编辑",
					id:id,			
					video:video[0]
				}); 							
			}
		}) ;
	}) ;
	//后台视频管理编辑
	app.post('/admin/video/update', function(req, res){
		var id = req.body.id ;
		var file = req.body.file ;
		var filename = req.body.filename ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		if(id){
			var data = {cate_id:1,file:file,filename:filename} ;
			News.update(id,data, function(err, result){
				if(err){
					res.send({status:"n",info:"更新失败"}) ;	
				}else{
					res.send({status:"y",info:"更新成功"}) ;	
				}
			}) ;
		}else{
			var data = {cate_id:1,file:file,filename:filename,created_at:created_at} ;
			News.save(data ,function(err, result){
				if(err){
					res.send({status:"n",info:"视频管理添加失败"}) ;	
				}else{
					res.send({status:"y",info:"视频管理添加成功"}) ;	
				}
			}) ;			
		}
	}) ;
	//后台视频管理删除
	app.post('/admin/video/delete', function(req, res){
		var qid = req.body.id ;
		News.deleteById(qid, function(err, result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除视频管理失败"}) ;
			}else{
				res.send({status:"y",info:"删除视频管理成功"}) ;				
			}
		}) ;
	}) ;

//============视频 end===========


//============图片 start===========


	//后台图片管理
	app.get('/admin/picture',function(req, res){
		var pid = req.query['pid'] ;
		var pageSize = 10 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		News.findAllByPage(24,currPage, pageSize, keywords, function(err,totalCount, picture){
			if(err){
				logger.info("find all error") ;
			}
			logger.info(" totalCount---->:"+totalCount) ;
			//var totalCount = users.length ;
			res.render('admin/picture',{
				title:"图片管理管理",
				picture:picture,
				pid:pid,
				keywords:keywords,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			}); 			
		}) ;
	}) ;
	//后台图片管理添加页
	app.get('/admin/picture/add',function(req,res){
		res.render('admin/picture-edit') ;
	}) ;
	//后台图片管理编辑页
	app.get('/admin/picture/edit/:id',function(req, res){
		var id = req.params.id ;
		News.findById(id, function(err, picture){
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			if(picture){
				logger.info("picture:"+picture) ;
				//console.log(picture[0]) ;
				res.render('admin/picture-edit',{
					title:"会员编辑",
					id:id,			
					picture:picture[0]
				}); 							
			}
		}) ;
	}) ;
	//后台图片管理编辑
	app.post('/admin/picture/update', function(req, res){
		var id = req.body.id ;
		var file = req.body.file ;
		var filename = req.body.filename ;
		var description = req.body.description ;
		var cate_id = req.body.cate_id ;

		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		if(id){
			var data = {cate_id:cate_id,file:file,filename:filename,description:description} ;
			News.update(id,data, function(err, result){
				if(err){
					res.send({status:"n",info:"更新失败"}) ;	
				}else{
					res.send({status:"y",info:"更新成功"}) ;	
				}
			}) ;
		}else{
			var data = {cate_id:cate_id,file:file,filename:filename,description:description,created_at:created_at} ;
			News.save(data ,function(err, result){
				if(err){
					res.send({status:"n",info:"图片管理添加失败"}) ;	
				}else{
					res.send({status:"y",info:"图片管理添加成功"}) ;	
				}
			}) ;			
		}
	}) ;
	//后台图片管理删除
	app.post('/admin/picture/delete', function(req, res){
		var qid = req.body.id ;
		News.deleteById(qid, function(err, result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除图片管理失败"}) ;
			}else{
				res.send({status:"y",info:"删除图片管理成功"}) ;				
			}
		}) ;
	}) ;






//============图片 end===========


//============答题卡 start===========
	//后台答题卡管理
	app.get('/admin/card',function(req, res){
		var pid = req.query['pid'] ;
		var pageSize = 10 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		News.findAllByPage(3,currPage, pageSize, keywords, function(err,totalCount, card){
			if(err){
				logger.info("find all error") ;
			}
			logger.info(" totalCount---->:"+totalCount) ;
			//var totalCount = users.length ;
			res.render('admin/card',{
				title:"答题卡管理管理",
				card:card,
				pid:pid,
				keywords:keywords,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			}); 			
		}) ;
	}) ;
	//后台答题卡管理添加页
	app.get('/admin/card/add',function(req,res){
		res.render('admin/card-edit') ;
	}) ;
	//后台答题卡管理编辑页
	app.get('/admin/card/edit/:id',function(req, res){
		var id = req.params.id ;
		News.findById(id, function(err, card){
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			if(card){
				logger.info("card:"+card) ;
				//console.log(card[0]) ;
				res.render('admin/card-edit',{
					title:"会员编辑",
					id:id,			
					card:card[0]
				}); 							
			}
		}) ;
	}) ;
	//后台答题卡管理编辑
	app.post('/admin/card/update', function(req, res){
		var id = req.body.id ;
		var title = req.body.title ;
		var file = req.body.file ;
		var filename = req.body.filename ;
		var description = req.body.description ;

		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		if(id){
			var data = {cate_id:3,title:title,file:file,filename:filename,description:description} ;
			News.update(id,data, function(err, result){
				if(err){
					res.send({status:"n",info:"更新失败"}) ;	
				}else{
					res.send({status:"y",info:"更新成功"}) ;	
				}
			}) ;
		}else{
			var data = {cate_id:3,title:title,file:file,filename:filename,description:description,created_at:created_at} ;
			News.save(data ,function(err, result){
				if(err){
					res.send({status:"n",info:"答题卡管理添加失败"}) ;	
				}else{
					res.send({status:"y",info:"答题卡管理添加成功"}) ;	
				}
			}) ;			
		}
	}) ;
	//后台答题卡管理删除
	app.post('/admin/card/delete', function(req, res){
		var qid = req.body.id ;
		News.deleteById(qid, function(err, result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除答题卡管理失败"}) ;
			}else{
				res.send({status:"y",info:"删除答题卡管理成功"}) ;				
			}
		}) ;
	}) ;
//============答题卡 end===========
}