/*
 * admin
 */
var http = require('http') ;
var path = require('path');
var fs = require('fs') ; 
var crypto = require('crypto') ;
var logger = require('../Logger.js');
var moment = require('moment');

var Admin = require('../models/admin.js') ;
var User = require('../models/user.js') ;
var Question = require('../models/question.js') ;
var Answer = require('../models/answer.js') ;
var PaperQuestion = require('../models/paper.question.js') ;
var Tag = require('../models/tag.js') ;
var Paper = require('../models/paper.js') ;
var Options = require('../models/options') ;
var Graded = require('../models/graded.js') ;
var Classification = require('../models/classification.js') ;
var Category = require('../models/category.js') ;
var UserLog = require('../models/user.log.js') ;
var Respondents = require('../models/respondents.js') ;
var Code = require('../models/code.js') ;
var ICVL  = require('iconv-lite');

var Pager = require('../Pager') ;
var StringUtils = require('../StringUtils') ;
var uuid = require('node-uuid') ;
var async = require('async') ;

module.exports = function(app) {
	//app.get('/admin',checkLogin) ;
	//test
	app.get('/admin/test', function(req, res) {
		res.setHeader('Content-Type','text/html');
		res.setHeader('charset','utf-8');

		res.send({status:"y",info:"上传成功","path":"","filename":""}) ;			
	});
	//后台欢迎
	app.get('/admin', function(req, res) {
		var user = new User();
		var id = req.session.admin?req.session.admin.id:"" ;
		if(!id){
			res.redirect("/admin/login");
		}
		user.find(id,function(err, user){
			if(err){
				console.log("err") ;
			}
			res.render('admin/index', {
				title: '后台',
				user:user[0]
			});
		});
	});
	//后台登陆页
	app.get('/admin/login', function(req, res){
		res.render('admin/login') ;
	}) ;
	//后台登陆
	app.post('/admin/loginAction', function(req, res){
		var username = req.body.username ;
		var password = req.body.password ;
		var mobileCode = req.body.code ;
		//生成口令的散列值
		//var md5 = crypto.createHash('md5'); 
		//var password = md5.update(password).digest('base64'); 

		Admin.findByUsername(username,function(err,admin){
			if(!!!admin[0]||!!!admin[0]['mobile']){
				res.send('mobile empty') ;
			}else{
				var mobile = admin[0]['mobile'] ;
				//手机验证码
				Code.findByMobileCode(mobile,mobileCode,function(err,code){
					if(err){
						//logger.error("find mobileCode err") ;
					}
					if(code.length>0){
						//判断密码是否正确
						if(admin[0]['password']==password){
							//验证码置为不可用
							Code.deleteByMobileCode(mobile, mobileCode, function(err,result){
								if(err){
									logger.info("deleteByMobileCode err") ;
								}
								//res.send("success") ;
								req.session.admin = admin[0]; 
								res.send({"info":'success'}) ;
							}) ;																						
						}else{
							res.send({"info":'pwderr'}) ;
						}
					}else{
						res.send({"info":'coderr'}) ;
					}
				});
			}
		}) ;

		/*
		Admin.find(username,password,function(err, admin){
			if(err){
				logger.info("username or password error") ;
				res.send({status:'n'}) ;
			}
			logger.info(admin) ;
			if(admin.length==0){
				res.send({status:'n'}) ;				
			}else{
				req.session.admin = admin[0]; 
				res.send({status:'y'}) ;
			}
		}) ;*/
	}) ;
	//后台退出
	app.get('/admin/logout',function(req, res){
		req.session.admin = null; 
		req.flash('success', '登出成功'); 
		res.redirect('/admin/login'); 
	}) ;
	//后台用户管理
	app.get('/admin/user',function(req, res){
		var pageSize = 10 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		User.findAll(currPage, pageSize, keywords, function(err,totalCount, users){
			if(err){
				logger.info("find all error") ;
			}
			logger.info("totalCount---->:"+totalCount) ;
			//var totalCount = users.length ;
			res.render('admin/users',{
				title:"会员管理",			
				users:users,
				keywords:keywords,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			}); 			
		}) ;
	}) ;
	//后台用户编辑页
	app.get('/admin/user/edit/:id',function(req, res){
		var id = req.params.id ;
		User.findById(id, function(err, user){
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			Classification.findAll(function(err,classification){
				if(err){
					logger.info("find graded error") ;
					res.send("error") ;
				}
				Graded.findAll(function(err,graded){
					if(err){
						logger.info("find graded error") ;
						res.send("error") ;
					}
					if(user){
						res.render('admin/users-edit',{
							title:"会员编辑",
							id:id,	
							graded:graded,
							classification:classification,
							user:user[0]
						}); 							
					}
				}) ;
			}) ;
		}) ;
	}) ;
	//后台用户编辑
	app.post('/admin/user/update', function(req, res){
		var nickname = req.body.nickname ;
		var taobao = req.body.taobao ;
		var alipay = req.body.alipay ;
		var is_fee = req.body.is_fee ;
		var id = req.body.id ;
		var graded = req.body.graded ;
		var classification = req.body.classification ;
		
		var settings = req.body.settings ;
		var power = 0 ;
		var class_power = 0 ;

		var exam_time = req.body.exam_time ;
		var address = req.body.address ;
		if(!!graded){
			graded.forEach(function(g){
				power += Math.pow(2,parseInt(g)) ;
			}) ;
		}
		if(!!classification){
			classification.forEach(function(g){
				class_power += Math.pow(2,parseInt(g)) ;
			}) ;
		}
		logger.info(power) ;
		User.update(id,nickname, taobao, alipay, is_fee, power, class_power, settings, exam_time, address , function(err, result){
			if(err){
				res.send({status:"n",info:"更新失败"}) ;	
			}else{
				res.send({status:"y",info:"更新成功"}) ;	
			}
		}) ;
	}) ;
	//后台用户删除
	app.post('/admin/user/delete', function(req, res){
		var uid = req.body.id ;
		UserLog.deleteByUid(uid,function(err,result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除用户失败"}) ;
			}else{
				Respondents.deleteByUid(uid,function(err,result){
					if(err){
						logger.info("delete err") ;
						res.send({status:"n",info:"删除用户失败"}) ;
					}else{
						User.deleteById(uid, function(err, result){
							if(err){
								logger.info("delete err") ;
								res.send({status:"n",info:"删除用户失败"}) ;
							}else{
								res.send({status:"y",info:"删除用户成功"}) ;				
							}
						}) ;
					}
				});
			}
		});
	}) ;
	//后台标签管理
	app.get('/admin/label',function(req, res){
		var pageSize = 10 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		Tag.findAllByPage(currPage, pageSize, keywords, function(err,totalCount, tags){
			if(err){
				logger.info("find all error") ;
			}
			logger.info(" totalCount---->:"+totalCount) ;
			//var totalCount = users.length ;
			res.render('admin/tags',{
				title:"标签管理",
				tags:tags,
				keywords:keywords,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			}); 			
		}) ;
	}) ;
	//后台标签添加页
	app.get('/admin/label/add',function(req,res){
		res.render('admin/tags-edit') ;
	}) ;
	//后台标签编辑页
	app.get('/admin/label/edit/:id',function(req, res){
		var id = req.params.id ;
		Tag.findById(id, function(err, tag){
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			if(tag){
				logger.info("tag:"+tag) ;
				//console.log(tag[0]) ;
				res.render('admin/tags-edit',{
					title:"会员编辑",
					id:id,			
					tag:tag[0]
				}); 							
			}
		}) ;
	}) ;
	//后台标签编辑
	app.post('/admin/label/update', function(req, res){
		var tagname = req.body.tagname ;
		var id = req.body.id ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		if(id){
			Tag.update(id,tagname, function(err, result){
				if(err){
					res.send({status:"n",info:"更新失败"}) ;	
				}else{
					res.send({status:"y",info:"更新成功"}) ;	
				}
			}) ;
		}else{
			Tag.save(tagname, created_at ,function(err, result){
				if(err){
					res.send({status:"n",info:"标签添加失败"}) ;	
				}else{
					res.send({status:"y",info:"标签添加成功"}) ;	
				}
			}) ;			
		}
	}) ;
	//后台标签删除
	app.post('/admin/label/delete', function(req, res){
		var qid = req.body.id ;
		Tag.deleteById(qid, function(err, result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除标签失败"}) ;
			}else{
				res.send({status:"y",info:"删除标签成功"}) ;				
			}
		}) ;
	}) ;

	//后台题库
	app.get('/admin/question',function(req, res){
		var cid = req.query['cid'] ;
		var pageSize = 10 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		var pid = !req.query['pid']?"":req.query['pid'] ;
		Question.findAll(currPage, pageSize, cid, keywords, function(err,totalCount, questions){
			if(err){
				logger.info("find all error") ;
			}
			logger.info("totalCount---->:"+totalCount) ;
			//var totalCount = users.length ;
			//查找所有标签
			Tag.findAll(function(err, tags){
				if(err){
					res.send("error") ;
				}
				//如果是添加试题到题库
				if(!!pid){
					Paper.findById(pid, function(err, paper){
						if(err){
							logger.info("findById error") ;
							res.send("error") ;
						}
						//查找已经加入试卷的题目
						PaperQuestion.findById(pid,'',function(err, paper_questions){
							logger.info("paper_questions:") ;
							logger.info(paper_questions) ;
							Category.findAll(function(err, category){
								if(err){
									logger.info("find category error") ;
									res.send("error") ;
								}
								res.render('admin/questions',{
									title:"题库管理",
									cid:cid,	
									pid:pid,		
									paper:paper[0],
									questions:questions,
									paper_questions:paper_questions,
									keywords:keywords,
									moment:moment,
									tags:tags,
									category:category,
									currPage:currPage,
									pageSize:pageSize,
									pager:Pager(req, totalCount, pageSize, currPage)
								}); 																
							}) ;
						}) ;

					}) ;
				}else{
					Category.findAll(function(err, category){
						if(err){
							logger.info("find category error") ;
							res.send("error") ;
						}
						res.render('admin/questions',{
							title:"题库管理",
							cid:cid,			
							questions:questions,
							keywords:keywords,
							moment:moment,
							tags:tags,
							category:category,
							currPage:currPage,
							pageSize:pageSize,
							pager:Pager(req, totalCount, pageSize, currPage)
						}); 								
					}) ;
				}
			}) ;
		}) ;
	}) ;
	//后台题目添加页
	app.get('/admin/question/add',function(req, res){
		var cid = req.query['cid'] ;
		Tag.findAll(function(err, tags){
			Category.findAll(function(err, category){
				res.render('admin/questions-add-edit',{
					title:"题目添加",
					tags:tags,
					category:category,
					cid:cid
				}); 																
			}) ;
		}) ;
	}) ;
	//后台题目编辑页
	app.get('/admin/question/edit/:id',function(req, res){
		var id = req.params.id ;
		Question.findById(id, function(err, question){
			//console.log(question[0]) ;
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			if(question){
				Tag.findAll(function(err, tags){
					if(err){
						res.send("error") ;
					}
					Answer.findByQuestionId(id, function(err, answers){
						//console.log("answers:"+answers) ;
						if(err){
							res.send("error") ;
						}
						Category.findAll(function(err, category){
							res.render('admin/questions-add-edit',{
								title:"题目编辑",
								id:id,			
								question:question[0],
								answers:answers,
								tags:tags,
								category:category
							}); 																			
						}) ;
					}) ;
				}) ;
			}
		}) ;
	}) ;
	//后台题目编辑
	app.post('/admin/question/update', function(req, res){
		var question_id = req.body.id ;
		var area_id = req.body.area_id ;
		var label = req.body.label ;
		var title = req.body.title ;
		var category_id = req.body.category_id ;
		var num = req.body.num ;
		var correct = req.body.correct ;
		var score = req.body.score ;
		var img = req.body.img ;
		var music = req.body.music ;
		var titlesound = req.body.titlesound ;
		var settings = req.body.settings ;

		var option_a = req.body.option_a ;
		var option_a_img = req.body.option_a_img ;
		var option_b = req.body.option_b ;
		var option_b_img = req.body.option_b_img ;
		var option_c = req.body.option_c ;
		var option_c_img = req.body.option_c_img ;
		var option_d = req.body.option_d ;
		var option_d_img = req.body.option_d_img ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		logger.info("question_id:"+question_id) ;
		logger.info("question_id type:"+ typeof question_id) ;
		logger.info(question_id!='undefined') ;
		if(question_id!='undefined'){
			logger.info("更新") ;
			//更新
			Question.update(question_id,area_id,label,title,category_id,num,correct,score,img,music,titlesound,settings ,function(err, result){
				if(err){
					logger.info(err) ;
					res.send({status:"n",info:"更新问题失败"}) ;	
				}else{
					Answer.update(question_id,option_a,option_a_img,"A",created_at,function(err, result){
						if(err){
							res.send({status:"n",info:"更新选项A失败"}) ;	
						}else{
							Answer.update(question_id,option_b,option_b_img,"B",created_at,function(err, result){
								if(err){
									res.send({status:"n",info:"更新选项B失败"}) ;	
								}else{
									Answer.update(question_id,option_c,option_c_img,"C",created_at,function(err, result){
										if(err){
											res.send({status:"n",info:"更新选项C失败"}) ;	
										}else{
											Answer.update(question_id,option_d,option_d_img,"D",created_at,function(err, result){
												if(err){
													res.send({status:"n",info:"更新选项D失败"}) ;	
												}else{
													res.send({status:"y",info:"更新成功"}) ;	
												}
											});																				
										}
									});									
								}
							});
						}
					}) ;
				}
			}) ;			
		}else{
			//创建
			Question.save(area_id, label, title, category_id, num, correct , score, img,music,titlesound,settings,created_at, function(err, question_id){
				//console.log(question_id) ;
				question_id = question_id.id ;
				if(err){
					res.send({status:"n",info:"添加题目失败"}) ;	
				}else{
					Answer.save(question_id, option_a,option_a_img,"A",created_at,function(err, result){
						if(err){
							res.send({status:"n",info:"添加选项A失败"}) ;	
						}else{
							Answer.save(question_id, option_b,option_b_img,"B",created_at,function(err, result){
								if(err){
									res.send({status:"n",info:"添加选项B失败"}) ;	
								}else{
									Answer.save(question_id, option_c,option_c_img,"C",created_at,function(err, result){
										if(err){
											res.send({status:"n",info:"添加选项C失败"}) ;	
										}else{
											Answer.save(question_id, option_d,option_d_img,"D",created_at,function(err, result){
												if(err){
													res.send({status:"n",info:"添加选项D失败"}) ;	
												}else{
													res.send({status:"y",info:"添加成功"}) ;	
												}
											});																				
										}
									});									
								}
							});
						}
					}) ;
				}
			}) ;			
		}
	}) ;
	//后台题目删除
	app.post('/admin/question/delete', function(req, res){
		var qid = req.body.id ;
		Question.deleteById(qid, function(err, result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除题目失败"}) ;
			}else{
				res.send({status:"y",info:"删除题目成功"}) ;				
			}
		}) ;
	}) ;

	//上传
	app.post('/admin/upload', function(req, res){
		res.setHeader('Content-Type','text/html');
		res.setHeader('charset','utf-8');
		var limitMaxSize = req.body.limitMaxSize ;
		var type = req.body.type ;
		logger.info("limitMaxSize:"+limitMaxSize) ;
		var upfile = req.files.upfile;
	    var files = [];
	    if (upfile instanceof  Array) {
	        files = upfile;
	    } else {
	        files.push(upfile);
	    }
	    var fileSize = files.length ;
	    for (var i = 0; i < fileSize; i++) {
	        var file = files[i];
	        logger.info("file"+file) ;
	        //console.log(file) ;
	        //console.log(file.headers) ;
	        var src_path = file.path;
	        var name = file.name;
	        var ext = name.split(".") ;
	        if(ext.length>1){
	        	ext = ext[1] ;
	        }
	        logger.info("ext:"+ext) ;
	        //var target_path = path.join(path.dirname(__dirname), "/public/uploads/" + name) ;
	        //StringUtils.mkdir('./public/uploads/'+type, function(err) {logger.info(err);});
	        StringUtils.mkdirSync('./public/uploads/'+type);
	        //var target_path = "./public/uploads/questions/" + name ;
	        var newFile = uuid.v1()+"."+ext ;
	        var target_path = "./public/uploads/"+type+"/" + newFile ;
	        logger.info("target_path:"+target_path) ;
	        logger.info("src_path:"+src_path) ;
	        
	        fs.rename(src_path, target_path, function (err) {
	            if (err) {
	            	throw err;
	            	res.send({status:"n",info:"上传失败"}) ;
	            }
	            logger.info("i:"+i) ;//为啥i=1
				res.send({status:"y",info:"上传成功","path":target_path,"filename":newFile}) ;	
	        });
	    }
	}) ;
	//关于我们
	app.get('/admin/aboutus', function(req, res){
		Options.findByName('aboutus', function(err, aboutus){
			if(err){
				console.log("err") ;
			}
			res.render('admin/aboutus', { 
				title: '',
				aboutus:aboutus
			}); 
		}) ;			
	}) ;
	//关于我们编辑
	app.post('/admin/aboutus/update', function(req, res){
		var aboutus = req.body.aboutus ;		
		Options.update('aboutus',aboutus, function(err, result){
			if(err){
				res.send({status:"n",info:"更新失败"}) ;
			}else{
				res.send({status:"y",info:"更新成功"}) ;	
			}
		}) ;
	}) ;
	//联系我们
	app.get('/admin/contactus', function(req, res){
		Options.findByName('contactus', function(err, contactus){
			if(err){
				console.log("err") ;
			}
			res.render('admin/contactus', { 
				title: '',
				contactus:contactus
			}); 
		}) ;			
	}) ;
	//联系我们编辑
	app.post('/admin/contactus/update', function(req, res){
		var contactus = req.body.contactus ;
		Options.update('contactus', contactus, function(err, result){
			if(err){
				res.send({status:"n",info:"更新失败"}) ;	
			}else{
				res.send({status:"y",info:"更新成功"}) ;	
			}
		}) ;
	}) ;
	//公告
	app.get('/admin/notice', function(req, res){
		Options.findByName('notice', function(err, notice){
			if(err){
				console.log("err") ;
			}
			res.render('admin/notice', { 
				title: '',
				notice:notice
			}); 
		}) ;			
	}) ;
	//公告编辑
	app.post('/admin/notice/update', function(req, res){
		var notice = req.body.notice ;
		Options.update('notice', notice, function(err, result){
			if(err){
				res.send({status:"n",info:"更新失败"}) ;	
			}else{
				res.send({status:"y",info:"更新成功"}) ;	
			}
		}) ;
	}) ;

	//生成发送保存手机验证码
	app.post('/admin/mobileCode', function(req, res){
		var username = req.query.username ;
		var code = req.body.imgcode ;
		var ip = StringUtils.getClientIp(req) ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		//验证码verifycode = vcode.toLowerCase();
		if(code.toLowerCase()!=req.session.verifycode){
			res.send("codeeror") ;
			return ;
		}else{
			req.session.verifycode = "" ;//验证成功清除图片验证码
		}
		//每个ip一天之内最多只能发送20次
		Code.findCountByIp(ip,function(err,resultIp){
			if(err){
				logger.error("find ip error") ;
			}	
			if(!!resultIp&&resultIp[0].c>20){
				res.send({"status":"n","info":"lt20"}) ;
			}else{
				Admin.findByUsername(username,function(err,result){
					if(!!!result[0]||!!!result[0]['mobile']){
						res.send('mobile empty') ;
					}else{
						var mobile = result[0]['mobile'] ;
						Code.findByMobile(mobile, function(err,result1){
							if(err){
								logger.error("save error") ;
							}
							codeSaveAndSendSms(mobile,ip,res) ;
							
						}) ;				
					}
				}) ;
			}
		}) ;
		
	}) ;

};

/**
 * 发送短信
 */
function sendSms(mobile, content, callback){
	var user_id = '1343' ;
	var pass = 'M1!QDy896p' ;
	var channelid = '974' ;
	if(!user_id||!pass||!channelid){
		return ;
	}
	
	var content = encodeURIComponent_GBK("验证码："+content) ;
	//callback(content) ;
	var api = "http://admin.sms9.net/houtai/sms.php?cpid="+user_id+"&password="+pass+"&channelid="+channelid+"&tele="+mobile+"&msg="+content;
	logger.info("api:"+api) ;
	
	http.get(api,function(res){
		logger.info("Got response: " + res.statusCode);
		var result = "" ;
		res.on('data', function(d) {
      		//logger.info(d) ;
      		logger.info(d.toString()) ;
	  		//response.send(d) ;
	  		if(d.toString().indexOf("success")>-1){
		  		callback('success') ;
	  		}else{
	  			callback(d.toString()) ;
	  		}
    	});	
	}).on('error',function(e){
		logger.error("Got error: " + e.message);
	});
}

function codeSaveAndSendSms(mobile,ip,res){
	var code = '' ;
	var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
	var items = "0123456789".split('') ;
	for(var i=0; i<4; i++){
	    var rnd = Math.random();
	    var item = Math.round(rnd * (items.length - 1));
	    code += items[item];
	}
	Code.save(mobile,ip,code,created_at,function(err,result){
		if(err){
			logger.error("save error") ;
		}
		sendSms(mobile,code, function(result2){
			//console.log("result2:"+result2) ;
			res.charset = 'utf-8';
			res.send(result2) ;
		}) ;
	})					
}

/**
 * 转码utf8 to gbk urlencoding
 *http://cnodejs.org/topic/50fb0178df9e9fcc58c565c9
 */
function encodeURIComponent_GBK(str)
{
  if(str==null || typeof(str)=='undefined' || str=='') 
    return '';

  var a = str.toString().split('');

  for(var i=0; i<a.length; i++) {
    var ai = a[i];
    if( (ai>='0' && ai<='9') || (ai>='A' && ai<='Z') || (ai>='a' && ai<='z') || ai==='.' || ai==='-' || ai==='_') continue;
    var b = ICVL.encode(ai, 'gbk');
    var e = ['']; // 注意先放个空字符串，最保证前面有一个%
    for(var j = 0; j<b.length; j++) 
      e.push( b.toString('hex', j, j+1).toUpperCase() );
    a[i] = e.join('%');
  }
  return a.join('');
}




function checkLogin(req, res, next) { 
	if(!req.session.admin) { 
		req.flash('error', '未登入'); 
		return res.redirect('/admin/login'); 
	} 
	next(); 
} 
function checkNotLogin(req, res, next) { 
	if (req.session.admin) { 
		req.flash('error', '已登入'); 
		return res.redirect('/admin'); 
	} 
	next(); 
}
