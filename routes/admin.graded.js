var logger = require('../Logger.js');
var moment = require('moment');
var Pager = require('../Pager') ;
var StringUtils = require('../StringUtils') ;
var uuid = require('node-uuid') ;
var async = require('async') ;
var Graded = require('../models/graded.js') ;

module.exports = function(app) {
	//后台等级管理
	app.get('/admin/graded',function(req, res){
		var pageSize = 10 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		Graded.findAllByPage(currPage, pageSize, keywords, function(err,totalCount, graded){
			if(err){
				logger.info("find all error") ;
			}
			logger.info(" totalCount---->:"+totalCount) ;
			//var totalCount = users.length ;
			res.render('admin/graded',{
				title:"等级管理",
				graded:graded,
				keywords:keywords,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			}); 			
		}) ;
	}) ;
	//后台等级添加页
	app.get('/admin/graded/add',function(req,res){
		res.render('admin/graded-edit') ;
	}) ;
	//后台等级编辑页
	app.get('/admin/graded/edit/:id',function(req, res){
		var id = req.params.id ;
		Graded.findById(id, function(err, tag){
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			if(tag){
				logger.info("tag:"+tag) ;
				//console.log(tag[0]) ;
				res.render('admin/graded-edit',{
					title:"会员编辑",
					id:id,			
					tag:tag[0]
				}); 							
			}
		}) ;
	}) ;
	//后台等级编辑
	app.post('/admin/graded/update', function(req, res){
		var tagname = req.body.tagname ;
		var id = req.body.id ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		if(id){
			Graded.update(id,tagname, function(err, result){
				if(err){
					res.send({status:"n",info:"更新失败"}) ;	
				}else{
					res.send({status:"y",info:"更新成功"}) ;	
				}
			}) ;
		}else{
			Graded.save(tagname, created_at ,function(err, result){
				if(err){
					res.send({status:"n",info:"等级添加失败"}) ;	
				}else{
					res.send({status:"y",info:"等级添加成功"}) ;	
				}
			}) ;			
		}
	}) ;
	//后台等级删除
	app.post('/admin/graded/delete', function(req, res){
		var qid = req.body.id ;
		Graded.deleteById(qid, function(err, result){
			if(err){
				logger.info("delete err") ;
				res.send({status:"n",info:"删除等级失败"}) ;
			}else{
				res.send({status:"y",info:"删除等级成功"}) ;				
			}
		}) ;
	}) ;
}