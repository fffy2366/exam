
/*
 * GET socket page.
 */
var http = require('http') ;
var logger = require('../Logger.js');
var moment = require('moment');
var StringUtils = require('../StringUtils') ;

module.exports = function(app) {
	var server = http.createServer(app) ;
	var io = require('socket.io').listen(server) ;
	app.get('/socket', function(req, res) {

		res.render('socket', {
			title:""
		});
	});
}