/**
 * 免费测试
 */
var http = require('http') ;
var logger = require('../Logger') ;
var moment = require('moment');
var Pager = require('../Pager') ;
var StringUtils = require('../StringUtils') ;
var uuid = require('node-uuid') ;
var async = require('async') ;

var Paper = require('../models/paper.js') ;
var Answer = require('../models/answer.js') ;
var PaperQuestion = require('../models/paper.question.js') ;
var Classification = require('../models/classification.js') ;
var Graded = require('../models/graded.js') ;
var Respondents = require('../models/respondents.js') ;

module.exports = function(app) {
	app.get('/free/list', function(req, res) {
		var pageSize = 5 ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		Paper.findAllByPageIsFree(currPage, pageSize, keywords,'is_free',1 ,function(err,totalCount, papers){
			if(err){
				logger.info("find all error") ;
			}
			logger.info(" totalCount---->:"+totalCount) ;
			res.render('free-list',{
				title:"试卷列表",
				papers:papers,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			}); 			
		})
	})
	//试卷
	app.get('/free/paper/:id',function(req, res){
		var id = req.params.id ;
		PaperQuestion.findById(id,'',function(err, paperQuestions){
			if(err){
				logger.info("find PaperQuestion error") ;
			}	
			logger.info("length:"+paperQuestions.length) ;
			res.render('free-test', {
				title: '分级',
				id:id,
				paperCount:paperQuestions.length
			});					
		});
	});
	//题目 前台ajax 获取第N道题目
	app.post('/free/paper-question',function(req, res){
		var paper_id = req.body.paper_id ;
		var id = req.body.id ;//第几道题
		var currPage = 1 ;
		var pageSize = 40 ;//每套试卷的题目数
		var keywords = "" ;
		var user_id = 1 ;
		logger.info("id:"+id) ;
		PaperQuestion.findAllByPage(paper_id,currPage, pageSize, keywords, function(err,totalCount, questions){
			if(err){
				logger.info("find all error") ;
			}
			var question = questions[(id-1)] ;
			//console.log(questions) ;
			//console.log(question) ;
			if(!!question){
				//题目选项
				Answer.findByQuestionId(question.question_id, function(err, answers){
					//console.log("answers:"+answers) ;
					if(err){
						res.send("error") ;
					}
					question.answers = answers ;
					//是否已经答过，如果答过回填
					Respondents.findById(user_id, paper_id, question.question_id, function(err, respondents){
						if(err){
							res.send("find by id respondents error") ;
						}
						question.respondents = respondents[0] ;
						res.send(question) ;
					}) ;					
				}) ;				
			}else{
				res.send("") ;
			}

		});
	}) ;
	//答题记录
	app.post('/free/respondents',function(req, res){
		var paper_id = req.body.paper_id ;
		var question_id = req.body.question_id ;
		var option = req.body.option ;
		var user_id = 1 ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		//logger.info("user_id:"+user_id) ;
		//logger.info(req.body) ;
		//判断是否
		Respondents.update(user_id, paper_id, question_id, option,created_at, function(err,respondents){
			if(err){
				logger.info("update error") ;
			}
			res.send("success") ;
		});
		
	}) ;
	//答题结果
	app.get('/free/testresult/:id',function(req, res){
		var user_id = 1 ;
		var id = req.params.id ;
		var currPage = 1 ;
		var pageSize = 40 ;//每套试卷的题目数
		var keywords = "" ;
		PaperQuestion.findAllByPage(id,currPage, pageSize, keywords, function(err,totalCount, questions){
			if(err){
				logger.info("find PaperQuestion error") ;
			}	
			logger.info("length:"+questions.length) ;
			var correct = [];
			var wrong = [] ;
			var score = 0 ;
			var i = 0 ;
			async.eachSeries(questions, function(q,callback){
				logger.info("q:"+q) ;
				//console.log(q) ;
				i++ ;	
				var a = function(i){
					Respondents.findById(user_id, id, q.question_id, function(err, respondents){
						if(err){
							res.send("find by id respondents error") ;
						}
						//logger.info("respondents[0].answer:"+respondents[0].answer) ;
						logger.info("q.correct:"+q.correct) ;
						if(!!respondents[0]&&respondents[0].answer==q.correct){
							logger.info("correct---->") ;
							correct.push(i) ;
							score = score+parseInt(q.score) ;
						}else{
							logger.info("wrong---->") ;
							wrong.push(i) ;
						}
						callback(err, correct, wrong) ;
					}) ;
				}
				a(i) ;											
				logger.info("i---->:"+i) ;
			}, function(err){
				if(err){
					logger.info(err) ;
				}else{				
					logger.info("render=====correct:"+correct+"wrong:"+wrong) ;	
					Paper.findById(id,function(err,paper){
						res.render('free-testresult', {
							title: '答题结果',
							id:id,
							paper:paper[0],
							correct:correct,
							wrong:wrong,
							score:score,
							paperCount:questions.length
						});										
					}) ;
				}
			});
		});
	});
	//删除考试记录
	app.post('/free/resettest',function(req, res){
		var user_id = 1 ;
		var paper_id = req.body.paper_id ;
		Respondents.delete(user_id,paper_id,function(err,results){
			if(err){
				logger.error('respondents delete err:'+err) ;
			}
			res.send({'status':'y','info':'success'}) ;
		}) ;
	}) ;
	
}