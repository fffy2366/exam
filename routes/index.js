/*
 * GET home page.
 */
var http = require('http') ;
var crypto = require('crypto') ;
var User = require('../models/user.js') ;
var logger = require('../Logger.js');
var moment = require('moment');
var Pager = require('../Pager') ;
var ICVL  = require('iconv-lite');
var Code = require('../models/code.js') ;
var StringUtils = require('../StringUtils') ;
var UserLog = require('../models/user.log.js') ;
var Options = require('../models/options') ;
var News = require('../models/news.js') ;

module.exports = function(app) {
	app.get('/', function(req, res) {
		var user = new User();
		user.find(1,function(err, user){
			if(err){
				console.log("err") ;
			}
			//console.log(user) ;
			logger.info("test!!!!!!!!!!!!!");
			res.render('index', {
				title: '首页',
				user:user[0]
			});
		});
	});
	/**
	 * landing page
	 */
	app.get('/landing', function(req, res) {
		res.render('landing', {
			title: 'landing page'
		});
	});

	//app.get('/reg', checkNotLogin);
	app.get('/reg', function(req, res) {
		res.render('reg', {
			title: '用户注册'
		}); 
	});
	app.post('/reg', function(req, res) { 
		var mobile = req.body.mobile ;
		var mobileCode = req.body.mobileCode ;
		var nickname = req.body.nickname ;
		var taobao = req.body.taobao ;
		var alipay = req.body.alipay ;
		var code = req.body.code ;
		var exam_time = req.body.exam_time ;
		var address = req.body.address ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		
		//生成口令的散列值
		var md5 = crypto.createHash('md5'); 
		var password = md5.update(req.body.password).digest('base64'); 
		//验证码verifycode = vcode.toLowerCase();
		logger.info(code) ;
		logger.info(req.session.verifycode) ;
		/*
		if(code.toLowerCase()!=req.session.verifycode){
			res.send("codeeror") ;
			return ;
		}*/		
		
		//手机验证码
		Code.findByMobileCode(mobile,mobileCode,function(err,code){
			if(err){
				logger.error("find mobileCode err") ;
			}
			if(code.length>0){
				//检查用户名是否已经存在
				User.get(mobile, function(err, user) { 
					//console.log(user) ;
					if(err) { 
						logger.error("err") ;
					} 
					if(user){
						res.send("mobileexist") ;
						return ;
					}else{
						//如果不存在则新增用户
						User.save(mobile,nickname,taobao,alipay,password,exam_time,address,created_at,function(err) { 
							if(err) { 
								logger.error("user save err") ;
							} 
							//验证码置为不可用
							Code.deleteByMobileCode(mobile, mobileCode, function(err,result){
								if(err){
									logger.info("deleteByMobileCode err") ;
								}
								res.send("success") ;
							}) ;																						
						}); 
						//验证码置为不可用
						Code.deleteByMobileCode(mobile, mobileCode, function(err,result){
							
						}) ;																						

					} 
				}); 				
			}else{
				res.send("mobileCodeError") ;
			}
		}) ;
	});
	app.get('/login', checkNotLogin);
	app.get('/login', function(req, res) { 
		var ip = StringUtils.getClientIp(req) ;
		logger.info("ip------------->:"+ip) ;
		var Pngjs = require('../pngjs') ;
		//Pngjs.create() ;
		res.render('login', { 
			title: '用户登入'
		}); 
	}); 
	//验证码
	app.get('/captcha',function(req, res){
		var Captcha = require('../captcha') ;		
		var c = new Captcha(req, res) ;
	}) ;
	//验证码ccap
	app.get('/ccap',function(req, res){
		var Ccap = require('../ccap') ;
		var c = new Ccap(req, res) ;
	}) ;
	//验证码1
	app.get('/captcha1',function(req, res){
		var Canvas = require('../canvas') ;
		var c = new Canvas(req, res) ;
	}) ;

	//生成发送保存手机验证码
	app.post('/mobileCode', function(req, res){
		var mobile = req.query.mobile ;
		var code = req.body.code ;
		var type = req.body.type ;
		var ip = StringUtils.getClientIp(req) ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		if(!!!mobile){
			res.send('mobile empty') ;
			return ;
		}
		
		//验证码verifycode = vcode.toLowerCase();
		if(code.toLowerCase()!=req.session.verifycode){
			res.send("codeeror") ;
			return ;
		}else{
			req.session.verifycode = "" ;//验证成功清除图片验证码
		}
		//每个ip一天之内最多只能发送20次
		Code.findCountByIp(ip,function(err,resultIp){
			if(err){
				logger.error("find ip error") ;
			}	
			if(!!resultIp&&resultIp[0].c>20){
				res.send({"status":"n","info":"lt20"}) ;
			}else{
				// TODO:每个手机 24小时内只能发送一个验证码，每个验证码只能使用一次
				Code.findByMobile(mobile, function(err,result1){
					if(err){
						logger.error("code find by mobile error") ;
					}
					User.get(mobile,function(err,result3){
						if(err){
							logger.error("get user error") ;
						}
						//是否设置短信发送次数
						if(!!result3&&!!result3.settings){
							var captcha_count = JSON.parse(result3.settings).captcha_count ;
							captcha_count = !!!captcha_count||captcha_count<1?1:parseInt(captcha_count) ;
							logger.info("captcha_count------------->:"+captcha_count) ;
							if(result1.length<captcha_count){
								codeSaveAndSendSms(type,mobile,ip,res) ;
								return ;
							}
							if(result1[captcha_count-1]&&!!result1[captcha_count-1].created_at&&moment().subtract('days', 1).valueOf()<result1[captcha_count-1].created_at.valueOf()){
								//logger.info(moment().subtract('days', 1).valueOf()) ;
								//logger.info(result1[0].created_at.valueOf()) ;
								res.send({"status":"n","info":"lt24h","captcha_count":captcha_count}) ;
							}else{
								codeSaveAndSendSms(type,mobile,ip,res) ;
							}
						}else{
							if(result1[0]&&!!result1[0].created_at&&moment().subtract('days', 1).valueOf()<result1[0].created_at.valueOf()){
								//logger.info(moment().subtract('days', 1).valueOf()) ;
								//logger.info(result1[0].created_at.valueOf()) ;
								res.send({"status":"n","info":"lt24h","captcha_count":1}) ;
							}else{
								codeSaveAndSendSms(type,mobile,ip,res) ;
							}					
						}
					}) ;
				}) ;
			}
		}) ;

	}) ;
	//用户登录
	app.post('/login', function(req, res) { 
		var mobile = req.body.mobile ;
		var code = req.body.code ;
		var mobileCode = req.body.mobileCode ;
		var ip = StringUtils.getClientIp(req) ;
		var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
		//生成口令的散列值
		var md5 = crypto.createHash('md5'); 
		var password = md5.update(req.body.password).digest('base64'); 
		//验证码verifycode = vcode.toLowerCase();
		logger.info(code) ;
		logger.info(req.session.verifycode) ;
		if(code.toLowerCase()!=req.session.verifycode){
			res.send("codeeror") ;
			return ;
		}
		logger.info("ip:"+req.connection.remoteAddress) ;
		//手机验证码
		//Code.findByMobileCode(mobile,mobileCode,function(err,code){
			//if(err){
				//logger.error("find mobileCode err") ;
			//}
			//if(code.length>0){
			//}else{
			//	res.send("mobileCodeError") ;
			//}
		//});
		//判断该账户该ip是否在其它地方登录过
		UserLog.findOneByMobile(mobile,function(err, log){
			if(err){
				logger.info("findOneByMobileIp err") ;
			}
			//一天只能有一个ip登录
			if(log.length>1&&(log[0].ip!=ip&&log[1].ip!=ip)){
				res.send("todayhasotheriplogin") ;
			}else{
				//判断账号是否正确
				User.get(mobile, function(err, user) { 
					logger.info(user) ;
					if(!!!user||user=="undefined") { 
						res.send("nouser") ;
					} 
					//logger.info(user.password) ;
					logger.info(password) ;
					if(!!!user||user.password != password) {
						logger.info(2) ;
						res.send("pwderr") ;
					}else{
						logger.info(3) ;
						if(user.is_fee==0){
							res.send("nofee") ;
						}else{
							req.session.user = user; 
							//保存登录日志
							UserLog.save(user.user_id,mobile,ip,created_at,function(err,result){
								if(err){
									logger.info("save user log err") ;
								}
								//验证码置为不可用
								Code.deleteByMobileCode(mobile, mobileCode, function(err,result){
									if(err){
										logger.info("deleteByMobileCode err") ;
									}
									res.send("success") ;
								}) ;																						
							}) ;
						}
					}
				});							
			}
		}) ;						
	}); 
	app.get('/logout', function(req, res) { 
		req.session.user = null; 
		req.flash('success', '登出成功'); 
		res.redirect('/'); 
	});	
	/**
	 * 关于我们
	 */
	app.get('/aboutus', function(req, res) { 
		Options.findByName('aboutus', function(err, aboutus){
			if(err){
				console.log("err") ;
			}
			res.render('aboutus', { 
				title: '',
				aboutus:aboutus
			}); 
		}) ;			
	});
	/**
	 * 联系我们
	 */
	app.get('/contactus', function(req, res) { 
		Options.findByName('contactus', function(err, contactus){
			if(err){
				console.log("err") ;
			}
			res.render('contactus', { 
				title: '',
				contactus:contactus
			}); 
		}) ;			
	});
	/**
	 * 视频分享
	 */
	app.get('/video', function(req, res) { 
		var pageSize = 5 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		News.findAllByPage(1,currPage, pageSize, keywords, function(err,totalCount, video){
			if(err){
				logger.info("find all error") ;
			}
			logger.info(" totalCount---->:"+totalCount) ;
			//var totalCount = users.length ;
			res.render('video',{
				video:video,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			});	
		}) ;		 
	});
	/**
	 * 图片赏析
	 */
	app.get('/picture', function(req, res) { 
		var cid = req.query['cid'] ;
		cid = !!cid?cid:2 ;
		var pageSize = 6 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		News.findAllByPage(cid,currPage, pageSize, keywords, function(err,totalCount, picture){
			if(err){
				logger.info("find all error") ;
			}
			res.render('images',{
				picture:picture,
				keywords:keywords,
				cid:cid,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			}); 			
		}) ;
	});
	/**
	 * 图片赏析
	 */
	app.get('/picture/info/:id', function(req, res) { 
		var id = req.params.id ;
		News.findById(parseInt(id), function(err, picture){
			if(err){
				logger.info("findById error") ;
				res.send("error") ;
			}
			if(picture){
				logger.info("picture:"+picture) ;
				//console.log(picture[0]) ;
				res.render('imagesinfo',{
					title:"会员编辑",
					id:id,			
					picture:picture[0]
				}); 							
			}
		}) ;

	});
	/**
	 * 答题卡下载
	 */
	app.get('/card', function(req, res) { 
		var pid = req.query['pid'] ;
		var pageSize = 6 ;
		logger.info("currPage---->:"+req.query.page) ;
		var currPage = !req.query['page']?"1":req.query['page'] ;
		var keywords = !req.query['keywords']?"":req.query['keywords'] ;
		News.findAllByPage(3,currPage, pageSize, keywords, function(err,totalCount, card){
			if(err){
				logger.info("find all error") ;
			}
			res.render('card',{
				card:card,
				keywords:keywords,
				moment:moment,
				currPage:currPage,
				pager:Pager(req, totalCount, pageSize, currPage)
			}); 			
		}) ;		
	});
	/**
	 * 修改密码
	 */
	app.get('/changepwd', function(req, res) {
		res.render('changepwd',{});
	});
	app.post('/changepwd', function(req, res) {
		//生成口令的散列值
		var md5 = crypto.createHash('md5');
		var old_password = md5.update(req.body.old_password).digest('base64');
		md5 = crypto.createHash('md5');
		var password = md5.update(req.body.password).digest('base64');
		var user_id = req.session.user.user_id ;
		//验证旧密码
		User.findById(user_id, function(err, user){
			if(user[0].password==old_password){
				//更新新密码
				User.updatePwd(user_id,password, function(err, result){
					if(err){
						res.send({status:"n",info:"更新失败"}) ;
					}else{
						res.send({status:"y",info:"更新成功"}) ;
					}
				}) ;
			}else{
				res.send({status:"n",info:"旧密码不正确"}) ;
			}
		});

	});
};
function checkLogin(req, res, next) {
	if(!req.session.user) { 
		req.flash('error', '未登入'); 
		return res.redirect('/login'); 
	} 
	next(); 
} 
function checkNotLogin(req, res, next) { 
	if (req.session.user) { 
		req.flash('error', '已登入'); 
		return res.redirect('/'); 
	} 
	next(); 
}
/**
 * 发送短信
 */
function sendSms(type,mobile, content, callback){
	var user_id = '1343' ;
	var pass = 'M1!QDy896p' ;
	var channelid = '974' ;
	//console.log("type:"+type);
	if(!!type&&type=="voice"){
		channelid = '17023' ;
	}
	if(!user_id||!pass||!channelid){
		return ;
	}
	
	var content = encodeURIComponent_GBK("验证码："+content) ;
	//callback(content) ;
	var api = "http://admin.sms9.net/houtai/sms.php?cpid="+user_id+"&password="+pass+"&channelid="+channelid+"&tele="+mobile+"&msg="+content;
	//console.log(api) ;
	logger.info("api:"+api) ;
	
	http.get(api,function(res){
		logger.info("Got response: " + res.statusCode);
		var result = "" ;
		res.on('data', function(d) {
      		//logger.info(d) ;
      		logger.info(d.toString()) ;
	  		//response.send(d) ;
            //Todo:接口数据记录日志
            logger.error(d.toString()) ;
            Code.updateSmsStatus(mobile, d.toString(),function(){
                if(d.toString().indexOf("success")>-1){
                    callback('success') ;
                }else{
                    callback(d.toString()) ;
                }
            }) ;

    	});	
	}).on('error',function(e){
		logger.error("Got error: " + e.message);
	});
}
function codeSaveAndSendSms(type,mobile,ip,res){
	var code = '' ;
	var created_at = moment().format("YYYY-MM-DD HH:mm:ss") ;
	var items = "0123456789".split('') ;
	for(var i=0; i<4; i++){
	    var rnd = Math.random();
	    var item = Math.round(rnd * (items.length - 1));
	    code += items[item];
	}
	Code.save(mobile,ip,code,created_at,function(err,result){
		if(err){
			logger.error("save error") ;
		}
		sendSms(type,mobile,code, function(result2){
			//console.log("result2:"+result2) ;
			res.charset = 'utf-8';
			res.send(result2) ;
		}) ;
	})					
}
/**
 * 转码utf8 to gbk urlencoding
 *http://cnodejs.org/topic/50fb0178df9e9fcc58c565c9
 */
function encodeURIComponent_GBK(str)
{
  if(str==null || typeof(str)=='undefined' || str=='') 
    return '';

  var a = str.toString().split('');

  for(var i=0; i<a.length; i++) {
    var ai = a[i];
    if( (ai>='0' && ai<='9') || (ai>='A' && ai<='Z') || (ai>='a' && ai<='z') || ai==='.' || ai==='-' || ai==='_') continue;
    var b = ICVL.encode(ai, 'gbk');
    var e = ['']; // 注意先放个空字符串，最保证前面有一个%
    for(var j = 0; j<b.length; j++) 
      e.push( b.toString('hex', j, j+1).toUpperCase() );
    a[i] = e.join('%');
  }
  return a.join('');
}